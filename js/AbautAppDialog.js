

$(document).on("pageinit", "#abaut-dialog", function(){

	var dlg = $("#abaut-dialog");
	var initialized = false;
	dlg.on("pagebeforeshow", function(){
		if(initialized == false) {
			$.ajax({
				url: "abaut.json",
				success:function(data) {
					var lilist = dlg.find("#abaut-license-li-tmpl").tmpl(data);
					var ul = dlg.find("#abaut-license-ul");
					ul.append(lilist);
										
					ul.listview("refresh");
				},
				error: function(response) {
					KTLab.Util.toast.show(response, 1500);
					KTLab.Util.toast.show("ライセンステキストの取得に失敗しました。。", 5000);
				},
				async:false,
				
			});
			initialized = true;
		}
	});
	dlg.on("pagebeforeshow", function(){
		
	});
});
var KTLab;
(function (KTLab) {
    var WeaponListview = (function () {
        function WeaponListview(ulElem) {
            this.ulElemId = null;
            this.weaponTimerIdToElement = {};
            this.prevGameTime = 0;
            this.infos = {};
            this.timers = {};
            this.ulElemId = ulElem;


            try  {
                KTLab.Event.AddListener("weapon-create", "w-listview", this, function (e, that) {
                    that.CreateListItem(e.id, e.iconid, e.repoptime, e.firstsubtime, e.loopsubtime);
                    that.SetCountdownDisplay();
                });

                KTLab.Event.AddListener("weapon-remove", "w-listview", this, function (eventdata, that) {
                    that.RemoveListItem(eventdata.id);
                });

                KTLab.Event.AddListener("weapon-reset", "w-listview", this, function (e, that) {
                    for (var id in that.weaponTimerIdToElement) {
                        that.RemoveListItem(id);
                    }
                    var ul = $(that.ulElemId);
                    ul.listview("refresh");
                });

                KTLab.Event.AddListener("weapon-toggle-enable", "w-listview", this, function (e, that) {
                    var info = that.infos[e.id];
                    info.ToggleEnable();
                    if (info.IsEnable()) {
                        KTLab.Event.Trigger("weapon-enabled", { id: e.id });
                    } else {
                        KTLab.Event.Trigger("weapon-disabled", { id: e.id });
                    }

                    // クッキーを読み取ってカウントダウン対象のアイコンにカウントダウンの文字列を追加する
//                    navigator.notification.vibrate(45);
                    that.SetCountdownDisplay();
                });

                KTLab.Event.AddListener("weapon-enabled", "w-listview", this, function (e, that) {
                    var li = that.weaponTimerIdToElement[e.id];
                    var icon = li.find("#weaponIcon");
                    li.find("#select-firstsubtime").selectmenu('enable');
                    li.fadeTo(250, 1.0);
                });

                KTLab.Event.AddListener("weapon-disabled", "w-listview", this, function (e, that) {
                    var li = that.weaponTimerIdToElement[e.id];
                    var icon = li.find("#weaponIcon");
                    li.find("#select-firstsubtime").selectmenu('disable');
                    li.fadeTo(250, 0.5);
                });

                KTLab.Event.AddListener("game-interval", "w-listview", this, function (eventdata, that) {
                    that.prevGameTime = eventdata.time;
                    for (var id in that.infos) {
                    }
                });
                KTLab.Event.AddListener("game-time-reset", "w-listview", this, function (eventdata, that) {
                    that.prevGameTime = eventdata.time;
                    for (var id in that.infos) {
                        that.Refresh(id);
                    }
                });

                KTLab.Event.AddListener("game-ready-started", "w-listview", this, function (e, that) {
                    for (var id in that.weaponTimerIdToElement) {
                        var li = that.weaponTimerIdToElement[id];
                        var info = that.infos[id];

                        var loopSubTime = parseInt(li.find("#select-loopsubtime").val());
                        info.SetLoopSubTime(loopSubTime * 1000);
                        var loopsubtext = loopSubTime;
                        li.find("#loopsub-time").text("+"+loopsubtext);

                        var remaintime = parseInt(li.find("#select-remaintime").val());
                        info.SetInterval(remaintime);

                        var remain = info.GetInterval() + info.GetFirstSubTime();
                        li.find("#remain-time").text(KTLab.MStoTimeString(remain));

                        li.find("#before-timer").css("display", "none");
                        li.find("#after-timer").css("display", "");

                        if (info.IsEnable() == false) {
                            li.fadeTo(250, 0.2);
                        }

                        that.SetIconClickAction(id, false);
                    }
                });
                KTLab.Event.AddListener("game-started", "w-listview", this, function (e, that) {
                    that.prevGameTime = e.time;

                    for (var id in that.weaponTimerIdToElement) {
                        var li = that.weaponTimerIdToElement[id];
                        var info = that.infos[id];

                        var loopSubTime = parseInt(li.find("#select-loopsubtime").val());
                        info.SetLoopSubTime(loopSubTime * 1000);
                        var loopsubtext = loopSubTime;
                        li.find("#loopsub-time").text("+"+loopsubtext);

                        var remaintime = parseInt(li.find("#select-remaintime").val());
                        info.SetInterval(remaintime);

                        if (id in that.timers && that.timers[id] != null) {
                            that.timers[id].Stop();
                        }

                        if (info.IsEnable()) {
                            var newTimer = info.CreateWeaponTimer();
                            newTimer.Start();
                            newTimer.AddTime(info.GetFirstSubTime());
                            that.timers[id] = newTimer;
                        } else {
                            that.timers[id] = null;
                        }
                        that.Refresh(id);

                        li.find("#before-timer").css("display", "none");
                        li.find("#after-timer").css("display", "");

                        if (info.IsEnable() == false) {
                            li.fadeTo(250, 0.2);
                        }

                        li.click({id:id}, function(data){
                        	KTLab.Event.Queue("ui-click-upbtn", data.data);
                        });



                        that.SetIconClickAction(id, false);
                    }
                });
                KTLab.Event.AddListener("game-ended", "w-listview", this, function (e, that) {
                    for (var id in that.timers) {
                        var t = that.timers[id];
                        if (t != null) {
                            t.Stop();
                            t.Release();
                            that.timers[id] = null;
                        }
                    }

                    for (var id in that.weaponTimerIdToElement) {
                        var li = that.weaponTimerIdToElement[id];
                        var info = that.infos[id];
                        li.find("#before-timer").css("display", "");
                        li.find("#after-timer").css("display", "none");

                        li.css("background-color", "#eee");

                        if (info.IsEnable() == false) {
                            li.fadeTo(250, 0.5);
                        } else {
                            li.fadeTo(250, 1.0);
                        }

                        that.SetIconClickAction(id, true);

                        li.unbind("click");

                        that.Refresh(id);
                    }
                });

                KTLab.Event.AddListener("weapon-time-interval", "w-listview", this, function (e, that) {
                    that.Refresh(e.id);
                });

                KTLab.Event.AddListener("weapon-time-changed", "w-listview", this, function (e, that) {
                    that.Refresh(e.id);
                });

                KTLab.Event.AddListener("weapon-time-end", "w-listview", this, function (e, that) {
                    that.weaponTimerIdToElement[e.id].css("background-color", "#eee");
                    that.Refresh(e.id);
                });

                KTLab.Event.AddListener("ui-click-upbtn", "w-listview", this, function (e, that) {
                    var i = that.infos[e.id];
                    if (i.IsEnable() == false) {
                        return;
                    }
                    var changedTime = 0;
                    if (e.id in that.timers && that.timers[e.id] != null && that.timers[e.id].IsRunning()) {
                        that.timers[e.id].AddTime(WeaponListview.WEAPON_WORKTIME_CHANGE_VALUE);
                        changedTime = that.timers[e.id].GetCurrentTime();
                    } else {
                        var newInterval = i.GetInterval() + WeaponListview.WEAPON_INTERVAL_CHANGE_VALUE;
                        i.SetInterval(newInterval);
                        changedTime = newInterval;
                    }
                    KTLab.Event.Queue("weapon-time-changed", { id: e.id, time: changedTime });
                });

                KTLab.Event.AddListener("ui-click-downbtn", "w-listview", this, function (e, that) {
                    var i = that.infos[e.id];
                    if (i.IsEnable() == false) {
                        return;
                    }
                    var changedTime = 0;
                    console.debug("ui-click-downbtn id=" + e.id + "exist timer? = " + (e.id in that.timers));
                    if (e.id in that.timers && that.timers[e.id] != null && that.timers[e.id].IsRunning()) {
                        that.timers[e.id].SubTime(WeaponListview.WEAPON_WORKTIME_CHANGE_VALUE);
                        changedTime = that.timers[e.id].GetCurrentTime();
                    } else {
                        var newInterval = i.GetInterval() - WeaponListview.WEAPON_INTERVAL_CHANGE_VALUE;
                        i.SetInterval(newInterval);
                        changedTime = newInterval;
                    }
                    KTLab.Event.Queue("weapon-time-changed", { id: e.id, time: changedTime });
                });

                var notifySendEvent = function (e, that) {
                    var type = that.infos[e.id].GetIconType();
                    console.debug(e.id + "のIconTypeは" + type);
                    switch (type) {
                        case KTLab.WeaponTimerInfo.Icon_Roke:
                            KTLab.Event.Queue("weapon-notify-roke-req", { id: e.id, progress: e.progress });
                            break;
                        case KTLab.WeaponTimerInfo.Icon_Sniper:
                            KTLab.Event.Queue("weapon-notify-sr-req", { id: e.id, progress: e.progress });
                            break;
                        case KTLab.WeaponTimerInfo.Icon_OS:
                            KTLab.Event.Queue("weapon-notify-os-req", { id: e.id, progress: e.progress });
                            break;
                        default:
                            throw "weapon-time-firstnotify: " + type + "は通知対応していません。";
                            break;
                    }

                    var li = that.weaponTimerIdToElement[e.id];
                    switch (e.progress) {
                        case 0:
                            li.css("background-color", "#84df9c");
                            break;
                        case 1:
                            li.css("background-color", "#e4bf5b");
                            break;
                        case 2:
                            li.css("background-color", "#fc8181");
                            break;

                        default:
                            break;
                    }
                };

                KTLab.Event.AddListener("weapon-time-firstnotify", "w-listview", this, notifySendEvent);
                KTLab.Event.AddListener("weapon-time-secondnotify", "w-listview", this, notifySendEvent);
                KTLab.Event.AddListener("weapon-time-finalnotify", "w-listview", this, notifySendEvent);
                KTLab.Event.AddListener("weapon-time-end", "w-listview", this, notifySendEvent);
            } catch (e) {
                console.debug(e.message);
            }
        }

        WeaponListview.prototype.SetCountdownDisplay = function() {
        	// クッキーの情報とinfosの情報を読み取って html情報を書き換える
        	var opt = $.cookie("opt");
        	var len = opt.prio.length;
        	var cdTarget = null;

        	if(opt.cdenable == false || len == 0) {

        	} else {
            	for(var i = 0; i < len; ++i) {
            		for(var m in this.infos) {
            			console.debug(m);
            			if(opt.prio[i] == m && opt.weapons[m].enable) {
            				cdTarget = m;
            				break;
            			}
            		}
            		if(cdTarget != null) {
            			break;
            		}
            	}
        	}

        	console.debug("cdTarget:"+cdTarget);
        	// cdTargetを元に表示の変更
        	for(var id in this.infos) {
        		console.debug(id);
        		if(this.weaponTimerIdToElement[id] == null) {
        			console.debug("not exist weapon");
        			continue;
        		}
        		if(id == cdTarget) {
        			this.weaponTimerIdToElement[id].find("#countdown-text").show();
        		} else {
        			this.weaponTimerIdToElement[id].find("#countdown-text").hide();
        		}
        	}

        }
        WeaponListview.prototype.Refresh = function (id) {
        	if(id == null || id == undefined)
        	{
        		return;
        	}
            var li = this.weaponTimerIdToElement[id];

            if(li === undefined || li == null) {
            	return;
            }


            var remain;
            if (id in this.timers && this.timers[id] != null && this.timers[id] != undefined && this.timers[id].IsRunning()) {
                var t = this.timers[id];
                remain = t.GetCurrentTime();

                if (this.prevGameTime - remain <= 0) {
                    t.Stop();
                    li.fadeTo(250, 0.5);
                }
            } else {
            	if(this.infos[id] == null || this.infos[id] == undefined)
            	{
            		return;
            	}
                remain = this.infos[id].GetInterval() + this.infos[id].GetFirstSubTime();
            }
            var repop = this.prevGameTime - remain ;

            li.find("#repop-time").text(KTLab.MStoTimeString(repop));
            li.find("#remain-time").text(KTLab.MStoTimeString(remain));
        };

        WeaponListview.prototype.SetLiToIconImg = function (li, iconType) {
            var iconurl = "img/wicon_" + iconType + ".png";
            var iconElem = li.find("#iconimage");
            if (iconElem.size() != 0) {
                li.find("#iconimage").attr('src', iconurl);
            } else {
                li.find("#weaponIcon").append('<img id="iconimage" src="' + iconurl + '">');
            }
        };

        /**
         * 武器リストの項目を生成します。
         * @param id
         * @param icon
         * @param repopTimems
         * @param firstSubTime
         * @param loopSubTime
         */
        WeaponListview.prototype.CreateListItem = function (id, icon, repopTimems, firstSubTime, loopSubTime) {

			var ul = $(this.ulElemId);

            var info = new KTLab.WeaponTimerInfo(id, icon, "#FFF", repopTimems, firstSubTime, loopSubTime);
            this.infos[id] = info;


            var remainTimes =
            [
            	{text:"3:00", val:180*1000},
            	{text:"2:00", val:120*1000},
            	{text:"1:30", val: 90*1000},
            	{text:"0:30", val: 30*1000},
            ];

            var tmpldata =
        	[
        	 	{iconname:icon, repop: "13:00", remain:repopTimems, remaintimes: remainTimes, firstsub:[0,5,7,10,15], loopsub:[0,1,2,3,4,5]},
            ];

            var li = $("#weapon_li").tmpl(tmpldata);

            this.weaponTimerIdToElement[id] = li;

            li.css("background-color", "#eee");
/*
            li.find("#TimeDown").click({ id: id, type: icon }, function (e) {
                console.debug("click OK! down");
                KTLab.Event.Queue("ui-click-downbtn", e.data);
                e.preventDefault();
            });
            li.find("#TimeUp").click({ id: id, type: icon }, function (e) {
                console.debug("click OK! up");
                KTLab.Event.Queue("ui-click-upbtn", e.data);
                e.preventDefault();
            });
*/
            li.attr('id', id);
            this.SetLiToIconImg(li, icon);
            this.SetIconClickAction(id, true);
            ul.append(li).trigger("create");


            var fstsel = li.find("#select-firstsubtime");
            fstsel.change({ info: info, sel: fstsel }, function (e) {
                var fst = parseInt(e.data.sel.val());
                e.data.info.SetFirstSubTime(fst * 1000);
                KTLab.Event.Queue("weapon-firstsubtime-changed", {id:info.GetId(), time:fst});
            });

            info.SetFirstSubTime(firstSubTime * 1000);
            fstsel.val(firstSubTime);
            fstsel.selectmenu('refresh');

            var loopsel = li.find("#select-loopsubtime");
            loopsel.change({ info: info, sel: loopsel }, function (e) {
                var fst = parseInt(e.data.sel.val());
                e.data.info.SetLoopSubTime(fst);
                KTLab.Event.Queue("weapon-loopsubtime-changed", {id:info.GetId(), time:fst});
            });


            info.SetLoopSubTime(loopSubTime * 1000);
            loopsel.val(loopSubTime);
            loopsel.selectmenu('refresh');

        };

        WeaponListview.prototype.SetIconClickAction = function (id, doset) {
            var li = this.weaponTimerIdToElement[id];
            var iconElem = li.find("#weaponIcon");
            iconElem.unbind("click");
            if (doset == true) {
                iconElem.bind("click", { id: id, info: this.infos[id] }, function () {
                    KTLab.Event.Queue("weapon-toggle-enable", { id: id });
                });
            }
        };

        WeaponListview.prototype.GetListItem = function (id) {
            return this.weaponTimerIdToElement[id];
        };

        WeaponListview.prototype.RemoveListItem = function (id) {
            console.debug("remove start!");
            var li = this.weaponTimerIdToElement[id];
            li.remove();
            if (id in this.weaponTimerIdToElement) {
                this.weaponTimerIdToElement[id] = null;
            }
            if (id in this.infos) {
            	this.infos[id].Release();
                this.infos[id] = null;
            }
            if (id in this.timers) {
                this.timers[id] = null;
            }
        };
        WeaponListview.WEAPON_INTERVAL_CHANGE_VALUE = 10000;
        WeaponListview.WEAPON_WORKTIME_CHANGE_VALUE = 1000;

        WeaponListview.prototype.ToJSON = function () {
        	var cnt = this.infos.length;
        	var tmp = 0;
        	var weapons = {};
        	for(var idx in infos) {
        		var i = infos[idx];
        		weapons[i.GetId()] =
        			{
        				icon:i.GetIconType()
        				, enable:i.IsEnable()
        				, time:i.GetInterval()
        				, firstsubtime:i.GetFirstSubTime()
        				, loopsubtime:i.GetLoopSubTime()
        			};

        		++tmp;
        	}

        	return weapons;
        }
//        WeaponListview.lihtml =
        return WeaponListview;
    })();
    KTLab.WeaponListview = WeaponListview;
})(KTLab || (KTLab = {}));

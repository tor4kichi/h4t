var KTLab;
(function (KTLab) {
    var WeaponTimerInfo = (function () {
        function WeaponTimerInfo(id, iconType, iconColor, intervalms, firstSubTimeMS, loopSubTimeMS) {
            this.IconType = 0;
            this.IconColor = "#FFF";
            this.soundInfo = null;
            this.vibrateInfo = null;
            this.IntervalMS = 0;
            this.firstSubTimeMS = 0;
            this.isEnable = true;
            console.debug("WeponItemInfo create");
            this.id = id;
            this.IconType = iconType;
            this.IconColor = iconColor;
            this.IntervalMS = intervalms;
            this.firstSubTimeMS = firstSubTimeMS;
            this.loopSubTimeMS = loopSubTimeMS;

            var lisName = id + "wt";

            KTLab.Event.AddListener("weapon-time-reset", lisName, this, function (e, that) {
                console.debug("weapon-time-reset called!");
                that.IntervalMS = e.time;
                KTLab.Event.Queue("weapon-time-changed", { id: e.id, time: that.IntervalMS });
            });
            KTLab.Event.AddListener("weapon-time-change", lisName, this, function (e, that) {
                console.debug("weapon-time-change called!");
                that.IntervalMS = e.time;
                KTLab.Event.Queue("weapon-time-changed", { id: e.id, time: that.IntervalMS });
            });
            console.debug("WeponItemInfo OK.");
        }
        
        WeaponTimerInfo.prototype.Release = function() {
        	KTLab.Event.RemoveListenerFromName(this.id + "wt");
        };
        
        WeaponTimerInfo.prototype.CreateWeaponTimer = function () {
            var timer = new KTLab.WeaponTimer(this.id, this.IntervalMS, this.firstSubTimeMS, this.loopSubTimeMS);
            return timer;
        };
        
        WeaponTimerInfo.prototype.GetId = function () {
        	return this.id;
        }

        WeaponTimerInfo.prototype.GetIconType = function () {
            return this.IconType;
        };

        WeaponTimerInfo.prototype.SetIconType = function (t) {
            if (0 <= t && t < WeaponTimerInfo.Icon_Max) {
                throw "アイコンタイプとして不正な値が指定されました：t=" + t;
            }
            this.IconType = t;
        };

        WeaponTimerInfo.prototype.GetFirstSubTime = function () {
            return this.firstSubTimeMS;
        };

        WeaponTimerInfo.prototype.SetFirstSubTime = function (t) {
            this.firstSubTimeMS = t;
        };

        WeaponTimerInfo.prototype.GetInterval = function () {
            return this.IntervalMS;
        };

        WeaponTimerInfo.prototype.SetInterval = function (t) {
            this.IntervalMS = t;
        };
        
        WeaponTimerInfo.prototype.GetLoopSubTime = function () {
            return this.loopSubTimeMS;
        };

        WeaponTimerInfo.prototype.SetLoopSubTime = function (t) {
            this.loopSubTimeMS = t;
        };

        WeaponTimerInfo.prototype.Enable = function () {
            this.isEnable = true;
        };

        WeaponTimerInfo.prototype.Disable = function () {
            this.isEnable = false;
        };

        WeaponTimerInfo.prototype.ToggleEnable = function () {
            this.isEnable = !this.isEnable;
        };
        WeaponTimerInfo.prototype.IsEnable = function () {
            return this.isEnable;
        };
        WeaponTimerInfo.procMan = null;

        WeaponTimerInfo.Icon_Roke = 0;
        WeaponTimerInfo.Icon_Sniper = 1;
        WeaponTimerInfo.Icon_OS = 2;
        WeaponTimerInfo.Icon_Railgun = 3;
        WeaponTimerInfo.Icon_Stick = 4;
        WeaponTimerInfo.Icon_Max = WeaponTimerInfo.Icon_Stick + 1;
        return WeaponTimerInfo;
    })();
    KTLab.WeaponTimerInfo = WeaponTimerInfo;
})(KTLab || (KTLab = {}));

var KTLab;
(function (KTLab) {
    var sound = (function () {
        function sound(id) {
            this.Id = null;
            this.instance = null;
            this.Id = id;
            this.load();
        }
        sound.getPath = function () {
            var str = location.pathname;
            var i = str.lastIndexOf('/');
            return str.substring(0, i + 1);
        };

        sound.Init = function () {
            if (createjs.Sound.BrowserDetect.isIOS || createjs.Sound.BrowserDetect.isAndroid) {
                document.addEventListener("deviceready", sound.onDeviceReady, false);
            } else {
                sound.onDeviceReady();
            }
        };
        sound.onDeviceReady = function () {
            sound.IsDeviceReady = true;
            createjs.Sound.initializeDefaultPlugins();
            console.debug("sound is ready!");

            createjs.Sound.addEventListener("loadfile", createjs.proxy(function(e){
            	console.debug(e);
            }), (this));

        };

        sound.prototype.load = function () {
            if (sound.IsDeviceReady && createjs.Sound.isReady()) {
                var src = this.Id;

                var path = /*sound.getPath() + */ src;
                console.debug("try load sound: "+path);
                createjs.Sound.registerSound({src:path, id:this.Id});
            }
        };

        sound.prototype.release = function () {
        	createjs.Sound.removeSound(this.Id);
        };

        sound.prototype.play = function () {
            if (sound.IsDeviceReady == false && !createjs.Sound.isReady()) {
                console.debug("音の再生に失敗。Deviceが準備出来ていません。");
                return;
            }

        	if(!createjs.Sound.loadComplete(this.Id))
        	{
        		this.load();
        	}

            if(createjs.Sound.loadComplete(this.Id))
            {
            	console.debug("tyr sound play:"+this.Id);
            	this.instance = createjs.Sound.play(this.Id);

	            this.instance.addEventListener("complete", function()
	            {
	            	console.debug("sound complete!");
	            });
            }
        };

        sound.prototype.pause = function () {
        	console.debug(this.instance);
            if (this.instance != null && this.instance != undefined && this.instance.playState != createjs.Sound.PLAY_FINISHED) {
            	try {
            		this.instance.pause();
            	} finally {
                	console.debug("sound pause error!!");
            	}
            	console.debug("sound["+this.Id+"] on paused");
            }
        };

        sound.prototype.resume = function () {
            if (this.instance != null && this.instance != undefined) {
            	this.instance.resume();
            	console.debug("sound["+this.Id+"] on resumed");
            }
        };
        sound.prototype.stop = function () {
        	if (this.instance != null && this.instance != undefined) {
            	this.instance.stop();
            	console.debug("sound["+this.Id+"] on stoped");
            }
        };

        sound.prototype.onSuccess = function () {
            console.debug("playAudio():Audio Success");
        };

        sound.prototype.onError = function (error) {
            alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
        };

        sound.prototype.setAudioPosition = function (position) {
            document.getElementById('audio_position').innerHTML = position;
        };
        sound.IsDeviceReady = false;
        sound.LoadReqItem = {};
        return sound;
    })();
    KTLab.sound = sound;
})(KTLab || (KTLab = {}));

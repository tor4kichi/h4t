var App;
(function (App) {
    function EventTypeRegist() {
        var e = KTLab.Event;

        e.Regist("app-ready");
        e.Regist("app-exit");

        e.Regist("soundplay");
        e.Regist("soundstop");

        e.Regist("vibration");
        e.Regist("vib-short");
        e.Regist("vib-middle");
        e.Regist("vib-long");
    }
    App.EventTypeRegist = EventTypeRegist;
})(App || (App = {}));

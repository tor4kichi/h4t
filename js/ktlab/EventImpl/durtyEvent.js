var EventImpl;
(function (EventImpl) {
    function EventTypeRegist() {
        var e = KTLab.Event;

        e.Regist("ui-click-start");
        e.Regist("ui-click-now");
        e.Regist("ui-click-reset");
        e.Regist("ui-click-pause");
        e.Regist("ui-click-resume");

        e.Regist("ui-click-setting");
        e.Regist("ui-click-allreset");

        e.Regist("ui-click-add");
        e.Regist("ui-click-remove");

        e.Regist("ui-click-icon");
        e.Regist("ui-click-upbtn");
        e.Regist("ui-click-downbtn");

        e.Regist("game-time-reset");
        e.Regist("game-ready-time-reset");

        e.Regist("game-ready-start");
        e.Regist("game-ready-started");
        e.Regist("game-ready-interval");
        e.Regist("game-ready-end");
        e.Regist("game-ready-ended");

        e.Regist("game-start");
        e.Regist("game-started");
        e.Regist("game-end");
        e.Regist("game-ended");
        e.Regist("game-interval");

        e.Regist("game-pause");
        e.Regist("game-resume");

        e.Regist("weapon-new-request");

        e.Regist("weapon-create");

        e.Regist("weapon-remove");
        e.Regist("weapon-reset");

        e.Regist("weapon-time-started");
        e.Regist("weapon-time-interval");
        e.Regist("weapon-time-firstnotify");
        e.Regist("weapon-time-secondnotify");
        e.Regist("weapon-time-finalnotify");
        e.Regist("weapon-time-end");

        e.Regist("weapon-time-reset");
        e.Regist("weapon-time-change");
        e.Regist("weapon-time-changed");
        
        e.Regist("weapon-firstsubtime-changed");
        e.Regist("weapon-loopsubtime-changed");
        

        e.Regist("weapon-enabled");
        e.Regist("weapon-disabled");
        e.Regist("weapon-toggle-enable");

        e.Regist("weapon-notify-roke-req");
        e.Regist("weapon-notify-roke-comp");
        e.Regist("weapon-notify-sr-req");
        e.Regist("weapon-notify-sr-comp");
        e.Regist("weapon-notify-os-req");
        e.Regist("weapon-notify-os-comp");
    }
    EventImpl.EventTypeRegist = EventTypeRegist;
})(EventImpl || (EventImpl = {}));

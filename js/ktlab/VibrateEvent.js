var KTLab;
(function (KTLab) {
    (function (Event) {
        var EventError = (function () {
            function EventError(mes) {
                this.message = null;
                this.message = mes;
            }
            return EventError;
        })();
        Event.EventError = EventError;

        var EventData = (function () {
            function EventData(typename, d) {
                this.eType = null;
                this.data = null;
                this.eType = typename;
                this.data = d;
            }
            return EventData;
        })();
        Event.EventData = EventData;

        var EventListener = (function () {
            function EventListener(name, data, cb) {
                this.name = name;
                this.data = data;
                this.callback = cb;
            }
            return EventListener;
        })();
        Event.EventListener = EventListener;

        var EventManager = (function () {
            function EventManager(maxProcTime) {
                this.Listeners = [];
                this.EventQueue = [];
                this.EventTypes = [];
                this.isProcessing = false;
                this.maxProcessTime = 300;
                this.maxProcessTime = maxProcTime;
            }
            EventManager.prototype.AddEventListener = function (eType, name, data, cb) {
                if (this.EventTypeValidation(eType) == false) {
                    console.warn("登録されていないイベントはリスナーを追加できません。etype=" + eType);
                    return;
                }

                var newlis = new EventListener(name, data, cb);
                if (eType in this.Listeners) {
                    this.Listeners[eType].push(newlis);
                } else {
                    this.Listeners[eType] = [newlis];
                }

                console.log("Event: AddListenername=" + name + ", eType=" + eType);
            };

            EventManager.prototype.RemoveEventListener = function (eType, name) {
                if (this.EventTypeValidation(eType) == false) {
                    throw new KTLab.Event.EventError("登録されていないイベントはリスナーを削除できません。etype=" + eType);
                }

                if (name == null || name == undefined) {
                    this.Listeners[eType] = null;
                } else {
                    var list = this.Listeners[eType];
                    var prevcnt = list.length;
                    for (var i in list) {
                        var lis = list[i];
                        if (lis.name == name) {
                            console.log("リスナー削除:name=" + name + ", Index=" + i);
                            this.Listeners[eType].splice(i, 1);
                            break;
                        }
                    }

                    if (prevcnt == list.length) {
                        console.log("イベントリスナーの削除に失敗 name=" + name);
                        return false;
                    }
                }
                return true;
            };

            EventManager.prototype.RemoveEventListenerWithName = function (name) {
                var listeningCount = 0;
                for (var eType in this.Listeners) {
                    var list = this.Listeners[eType];
                    for (var i in list) {
                        var lis = list[i];
                        if (lis.name == name) {
                            console.log("Event:リスナー削除:name=" + name + ", event=" + eType);
                            this.Listeners[eType].splice(i, 1);
                            listeningCount++;
                            break;
                        }
                    }
                }
                console.log("Event:リスナー名:<" + name + ">の[" + listeningCount + "]個のイベントリスニングを解除しました。");
            };

            EventManager.prototype.QueueEvent = function (e) {
                this.EventQueue.push(e);
            };

            EventManager.prototype.TriggerEvent = function (e) {
                this.ProcessEvent(e);
            };

            EventManager.prototype.RegistEvent = function (eType) {
                if (this.EventTypeValidation(eType) == true) {
                    console.log(eType + "はすでに登録されたイベントタイプです");
                    return;
                }
                this.EventTypes[eType] = true;
                console.log("イベントを登録。eType=" + eType);
            };

            EventManager.prototype.EventTypeValidation = function (eType) {
                if (eType in this.EventTypes && this.EventTypes[eType] != null && this.EventTypes[eType] != undefined && this.EventTypes[eType] == true) {
                    return true;
                }
                return false;
            };

            EventManager.prototype.Process = function () {
                if (this.isProcessing) {
                    return;
                }

                this.isProcessing = true;
                var procCount = 0;
                var endTime = new Date().getTime() + this.maxProcessTime;
                while (endTime >= new Date().getTime() && this.EventQueue.length != 0) {
                    var e = this.EventQueue.shift();
                    this.ProcessEvent(e);

                    procCount = procCount + 1;
                }
                if (procCount > 0) {
                    console.log("Event:processed! Amount=" + procCount);
                }
                this.isProcessing = false;
            };

            EventManager.prototype.IsProcessing = function () {
                return this.isProcessing;
            };

            EventManager.prototype.ProcessEvent = function (e) {
                if (this.EventTypeValidation(e.eType) == false) {
                    throw new EventError("イベントブロードキャスト失敗。イベントが登録されていません。eventType=" + e.eType);
                }

                var lisList = this.Listeners[e.eType];
                if (lisList == null || lisList == undefined || lisList.length == 0) {
                    console.log("Event:Exit Proc, Not Exist Listener! type=" + e.eType);
                    return false;
                }

                var lisCount = lisList.length;
                var lastLis = null;
                for (var i in lisList) {
                    try  {
                        var lis = lisList[i];
                        console.log("  RecieveEvent: " + lis.name);
                        lastLis = lis;
                        var ret = lis.callback(e.data, lis.data);
                    } catch (e) {
                        console.log(e.message);
                        console.log("イベントリスナーが例外を出しました。Listener=" + lastLis.name);
                    }
                }

                return true;
            };

            EventManager.prototype.OutputEventListeners = function (eType) {
                console.log("EventListener output start!");
                console.log("***");

                if (eType != undefined && eType != null) {
                    if (this.EventTypeValidation(eType) == false) {
                        console.log("Event.OutputEventListeners: 存在しないイベントタイプ.");
                    }

                    var lisList = this.Listeners[eType];
                    console.log("eType:" + type);

                    for (var lisId in lisList) {
                        var lis = lisList[lisId];
                        console.log("  " + lisId + "." + lis.name);
                    }
                } else {
                    for (var type in this.Listeners) {
                        var lisList = this.Listeners[type];
                        console.log("eType:" + type);

                        for (var lisId in lisList) {
                            var lis = lisList[lisId];
                            console.log("  " + lisId + "." + lis.name);
                        }
                    }
                }
                console.log("***");
                console.log("EventListener output Finish!");
            };
            return EventManager;
        })();
        Event.EventManager = EventManager;

        Event.EM = new EventManager(300);

        function Trigger(etype, data) {
            Event.EM.TriggerEvent(new EventData(etype, data));
        }
        Event.Trigger = Trigger;

        function Queue(etype, data) {
            Event.EM.QueueEvent(new EventData(etype, data));
        }
        Event.Queue = Queue;

        function Regist(type) {
            Event.EM.RegistEvent(type);
        }
        Event.Regist = Regist;
        function AddListener(type, name, data, callback) {
            Event.EM.AddEventListener(type, name, data, callback);
        }
        Event.AddListener = AddListener;

        function RemoveListener(type, name) {
            Event.EM.RemoveEventListener(type, name);
        }
        Event.RemoveListener = RemoveListener;

        function RemoveListenerFromName(name) {
            Event.EM.RemoveEventListenerWithName(name);
        }
        Event.RemoveListenerFromName = RemoveListenerFromName;

        function OutputEventListeners(eType) {
            Event.EM.OutputEventListeners(eType);
        }
        Event.OutputEventListeners = OutputEventListeners;
    })(KTLab.Event || (KTLab.Event = {}));
    var Event = KTLab.Event;
})(KTLab || (KTLab = {}));
var App;
(function (App) {
    function RegistVibrateEventListener(vibrateFunc) {
    }
    App.RegistVibrateEventListener = RegistVibrateEventListener;

    var VibrateManager = (function () {
        function VibrateManager(shortVibTime, midVibTime, longVibTime) {
            this.ShortVibTime = 50;
            this.MiddleVibTime = 100;
            this.LongVibTime = 200;
            this.ShortVibTime = shortVibTime;
            this.MiddleVibTime = midVibTime;
            this.LongVibTime = longVibTime;

            KTLab.Event.AddListener("vib-short", "appvibrate", this, function (e, that) {
                KTLab.Event.Queue("vibration", { time: that.ShortVibTime });
            });
            KTLab.Event.AddListener("vib-middle", "appvibrate", this, function (e, that) {
                KTLab.Event.Queue("vibration", { time: that.MiddleVibTime });
            });
            KTLab.Event.AddListener("vib-long", "appvibrate", this, function (e, that) {
                KTLab.Event.Queue("vibration", { time: that.LongVibTime });
            });
        }
        return VibrateManager;
    })();
    App.VibrateManager = VibrateManager;
})(App || (App = {}));

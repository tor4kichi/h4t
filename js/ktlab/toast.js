var KTLab;
(function (KTLab) {
	var Util;
	(function(Util) {
	
	var toast=function(fontsize, jqthemetype){
		this.themes;
		this.fontsize;
		this.showtime = 3000;
		
		if(fontsize ==null || fontsize == undefined) {
			this.fontsize = "10px";
		} else {
			this.fontsize = fontsize;
		}
		if(jqthemetype ==null || jqthemetype == undefined) {
			this.themes = "d";
		} else {
			this.themes = jqthemetype;
		}
		this.themes = jqthemetype;
	};
	
	toast.prototype = {
		show : function(msg, showtime, forceshow) {
			if(showtime == null || showtime == undefined) {
				showtime = this.showtime;
			}
			this.queue.Enqueue([msg, showtime]);
			
			if(this.__nowshowing == true) {
				return;
			}
			else {
				this.__next();
			}
		},
		queue : new KTLab.Queue(),
		
		__nowshowing : false,
		__next : function() {
			var _this = this;
			if(this.queue.Count() != 0) {
				var queuedItem = this.queue.Dequeue();
				var msg = queuedItem[0];
				var showtime = queuedItem[1];
				_this.__nowshowing = true;
				$("<div class='ui-loader ui-overlay-shadow ui-body-"+this.themes+" ui-corner-all'><h3>"+msg+"</h3></div>")
				.css({ display: "block", 
					"font-size": this.fontsize,
					opacity: 0.90, 
					position: "fixed",
					padding: "7px",
					"text-align": "center",
					width: "270px",
					left: ($(window).width() - 284)/2,
					top: $(window).height()*0.8 })
				.appendTo( $.mobile.pageContainer ).delay( showtime )
				.fadeOut( 400, function(){
					$(this).remove();
					_this.__nowshowing = false;
					_this.__next();
				});
			}
		}
	}
	Util.toast = new toast("9px", "a");
	})(Util || (Util = {}));
	KTLab.Util = Util;
})(KTLab || (KTLab = {}));
var KTLab;
(function (KTLab) {
    var ChainProcess = (function () {
        function ChainProcess(waittime, func, item) {
            this.WaitTime = waittime;
            this.Process = func;
            this.Item = item;
            this.isProcessing = false;
        }
        return ChainProcess;
    })();
    KTLab.ChainProcess = ChainProcess;

    var ChainProcessManager = (function () {
        function ChainProcessManager() {
            this.TimerId = -1;
            this.procQueue = new KTLab.Queue();
            this.Interval = 100;
        }
        ChainProcessManager.prototype.popProc = function () {
            if (this.procQueue.Count() > 0) {
                return this.procQueue.Dequeue();
            } else {
                return null;
            }
        };

        ChainProcessManager.prototype.Add = function (waittime, f, item) {
            this.procQueue.Enqueue(new ChainProcess(waittime, f, item));
            console.debug("ChainProcess: item count= " + this.procQueue.Count());
        };

        ChainProcessManager.prototype.Clear = function() {
        	this.procQueue.Clear();
        };

        ChainProcessManager.prototype.Start = function (interval) {
            var _this = this;
            if (this.TimerId != -1) {
                console.debug("ChainProcess: Alleady Started! TimerId= " + this.TimerId);
                return;
            }

            this.isProcessing = true;
            this.Interval = interval;
            this.TimerId = setInterval(function () {
            	if(_this.isProcessing == false) {
            		if(_this.TimerId != -1) {
            			clearInterval(_this.TimerId);
            			_this.TimerId = -1;
            		}
            		return;
            	}
                var proc = _this.popProc();

                if (proc == null) {
                    return;
                }

                if(_this.TimerId != -1) {
        			clearInterval(_this.TimerId);
        			_this.TimerId = -1;
        		}

                setTimeout(function () {
                	if( _this.isProcessing == true) {
                        _this.Start(_this.Interval);
                	}
                }, proc.WaitTime);

                console.debug("ChainProcess: proc! count= " + _this.procQueue.Count());
                proc.Process(proc.Item);
            }, interval, this);

            console.debug("ChainProcess: Start! TimerId= " + this.TimerId);
        };

        ChainProcessManager.prototype.Stop = function () {
            if (this.TimerId != -1) {
                clearInterval(this.TimerId);
            }
            this.TimerId = -1;
            this.isProcessing = false;
        };

        ChainProcessManager.prototype.IsRunning = function () {
            return this.TimerId != -1;
        };
        return ChainProcessManager;
    })();
    KTLab.ChainProcessManager = ChainProcessManager;
})(KTLab || (KTLab = {}));

var KTLab;
(function (KTLab) {
    var Queue = (function () {
        function Queue() {
            this.que = new Array();
        }
        Queue.prototype.Dequeue = function () {
            if (this.que.length > 0) {
                return this.que.shift();
            }
            return null;
        };

        Queue.prototype.Enqueue = function (o) {
            this.que.push(o);
        };

        Queue.prototype.Count = function () {
            return this.que.length;
        };

        Queue.prototype.Clear = function () {
            this.que.length = 0;
        };

        Queue.prototype.ToString = function () {
            return '[' + this.que.join(',') + ']';
        };
        return Queue;
    })();
    KTLab.Queue = Queue;
})(KTLab || (KTLab = {}));

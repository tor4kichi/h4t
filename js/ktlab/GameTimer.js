var KTLab;
(function (KTLab) {
    (function (H4Timer) {
        var GameTimerInfo = (function () {
            function GameTimerInfo(initGameTimeMinute) {
                this.gameTimeMS = KTLab.MinuteToMS(15);
                this.readyTimeMS = 3000;
                this.gt = null;
                this.gameTimeMS = initGameTimeMinute;
                KTLab.Event.AddListener("game-time-reset", "gameTimer", this, function (e, that) {
                    var t = e.time;
                    console.debug("GameTimeReset! time=" + t);
                    that.gameTimeMS = t;
                });
                KTLab.Event.AddListener("game-ready-time-reset", "gameTimer", this, function (e, that) {
                    var t = e.time;
                    console.debug("ReadyTimeReset! time=" + t);
                    that.readyTimeMS = t;
                });

                KTLab.Event.AddListener("game-end", "gameTimer", this, function (e, that) {
                    if (that.gt != undefined && that.gt != null) {
                        that.gt.Stop();
                        that.gt = null;
                    }
                });
                KTLab.Event.AddListener("game-pause", "gameTimer", this, function (e, that) {
                    if (that.gt != undefined && that.gt != null) {
                        that.gt.Pause();
                    }
                });
                KTLab.Event.AddListener("game-resume", "gameTimer", this, function (e, that) {
                    if (that.gt != undefined && that.gt != null) {
                        if (that.gt.IsRunning()) {
                            that.gt.Start();
                        }
                    }
                });
            }
            GameTimerInfo.prototype.InternalCreateGameTimer = function (withoutReadyTime) {
                var _this = this;
                var t = this.gameTimeMS;
                if (withoutReadyTime == false) {
                    t += this.readyTimeMS;
                }
                var timer = new KTLab.CountdownTimer("gameTimer", t, function () {
                });
                timer.SetStartCallback(function () {
                    if (withoutReadyTime) {
//                        KTLab.Event.Queue("game-started", {time:timer.GetCurrentTime()});
                    } else {
                        KTLab.Event.Queue("game-ready-started", null);
                    }
                });
                timer.SetIntervalCallback(function (curtime) {
                    var readyTime = curtime - _this.gameTimeMS;
                    if (withoutReadyTime == false && readyTime >= 0) {
                        console.debug("readyTime = " + readyTime);
                        if (readyTime == 0) {
                            KTLab.Event.Queue("game-ready-end", null);
                            KTLab.Event.Queue("game-started", { time: curtime });
                        } else {
                            KTLab.Event.Queue("game-ready-interval", { time: curtime, readytime: readyTime });
                        }
                    } else {
                        KTLab.Event.Queue("game-interval", { time: curtime });
                    }
                });
                timer.SetEndCallback(function () {
                    KTLab.Event.Queue("game-end", null);
                });

                this.gt = timer;
                return timer;
            };

            GameTimerInfo.prototype.CreateGameTimer = function () {
                return this.InternalCreateGameTimer(false);
            };

            GameTimerInfo.prototype.CreateGameTimerNoReady = function () {
                return this.InternalCreateGameTimer(true);
            };
            GameTimerInfo.prototype.GetGameTimeMS = function () {
                return this.gameTimeMS;
            };

            GameTimerInfo.prototype.GetReadyTimeMS = function () {
                return this.readyTimeMS;
            };
            return GameTimerInfo;
        })();
        H4Timer.GameTimerInfo = GameTimerInfo;
    })(KTLab.H4Timer || (KTLab.H4Timer = {}));
    var H4Timer = KTLab.H4Timer;
})(KTLab || (KTLab = {}));

var Test;
(function (Test) {
    function EventTest() {
        try  {
            KTLab.Event.Regist("test-event");
        } catch (e) {
            console.log(e.message);
            return;
        }

        try  {
            KTLab.Event.AddListener("test-event", "test-lis1", "lisdata-1", function (edata, lisdata) {
                console.log("edata:" + edata + ",lisdata:" + lisdata);
            });
            KTLab.Event.AddListener("test-event", "test-lis2", "lisdata-2", function (edata, lisdata) {
                console.log("edata:" + edata + ",lisdata:" + lisdata);
            });
        } catch (e) {
            console.log(e.message);
            return;
        }

        try  {
            KTLab.Event.Queue("test-event", "queueEvent called!");
        } catch (e) {
            console.log(e.message);
            return;
        }

        setInterval(function (data) {
            KTLab.Event.EM.Process();
        }, 100);
    }
    Test.EventTest = EventTest;
})(Test || (Test = {}));

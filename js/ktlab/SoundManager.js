var KTLab;
(function (KTLab) {
    var sound = (function () {
        function sound(id) {
            this.Id = null;
            this.media = null;
            this.Id = id;
            this.load();
        }
        sound.getPath = function () {
            var str = location.pathname;
            var i = str.lastIndexOf('/');
            return str.substring(0, i + 1);
        };

        sound.Init = function () {
            if (typeof device === 'undefined') {
                document.addEventListener("deviceready", sound.onDeviceReady, false);
            } else {
                sound.onDeviceReady();
            }
        };
        sound.onDeviceReady = function () {
            sound.IsDeviceReady = true;
            if (sound.LoadReqItem.length > 0) {
                for (var i in sound.LoadReqItem) {
                    var s = sound.LoadReqItem[i];
                    console.log("sound loading :" + s.Id);
                    s.load();
                }
                sound.LoadReqItem.length = 0;
            }
        };

        sound.prototype.load = function () {
            if (sound.IsDeviceReady && this.media == null) {
                var src = this.Id;

                var path = sound.getPath() + src;
                this.media = new Media(path, this.onSuccess, this.onError);
            } else if (this.media == null) {
                if (this.Id in sound.LoadReqItem == false) {
                    sound.LoadReqItem[this.Id] = this;
                }
            }
        };

        sound.prototype.release = function () {
            if (this.media != null) {
                this.media.stop();
                this.media.release();
                this.media = null;
            }
        };

        sound.prototype.play = function () {
            if (this.IsDeviceReady == false) {
                console.log("音の再生に失敗。Deviceが準備出来ていません。");
                return;
            }

            this.load();

            if (this.media == null) {
                return;
            }

            this.media.play({ numberOfLoops: 0 });
        };

        sound.prototype.pause = function () {
            if (this.media == null) {
                console.log("Error stop on not play sound:name=" + this.Id);
                return;
            }

            this.media.pause();
        };

        sound.prototype.stop = function () {
            if (this.media == null) {
                console.log("Error stop on not play sound:name=" + this.Id);
                return;
            }

            this.media.stop();
        };

        sound.prototype.onSuccess = function () {
            console.log("playAudio():Audio Success");
        };

        sound.prototype.onError = function (error) {
            alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
        };

        sound.prototype.setAudioPosition = function (position) {
            document.getElementById('audio_position').innerHTML = position;
        };
        sound.IsDeviceReady = false;
        sound.LoadReqItem = {};
        return sound;
    })();
    KTLab.sound = sound;
})(KTLab || (KTLab = {}));

var App;
(function (App) {
    var SoundManager = (function () {
        function SoundManager() {
            this.audioItems = {};
            KTLab.sound.Init();
            KTLab.Event.AddListener("soundplay", "appsound", this, function (e, that) {
                var n = e.name;
                if (n in that.audioItems) {
                    that.audioItems[n].play();
                } else {
                    that.audioItems[n] = new KTLab.sound(n);
                    that.audioItems[n].play();
                }
            });
            KTLab.Event.AddListener("soundstop", "appsound", this, function (e, that) {
                if (e.id in that.audioItems) {
                    that.audioItems[e.id].stop();
                }
            });
        }
        return SoundManager;
    })();
    App.SoundManager = SoundManager;
})(App || (App = {}));

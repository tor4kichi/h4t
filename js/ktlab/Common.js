var KTLab;
(function (KTLab) {
    KTLab.MinuteToMS = function (min) {
        return min * 60 * 1000;
    };

    KTLab.MStoTimeString = function (time) {
        if (time < 0) {
            return "00:00";
        }
        var curTimeSec = Math.ceil(time / 1000);
        var min = Math.floor(curTimeSec / 60);
        var sec = curTimeSec % 60;
        var minTop = (min < 10) ? '0' : '';
        var secTop = (sec < 10) ? '0' : '';
        return minTop + min + ':' + secTop + sec;
    };

    KTLab.ArrayCount = function (ary) {
        var cnt = 0;
        for (var i in ary) {
            cnt++;
        }

        return cnt;
    };
    
 // インタフェースクラス
	var Interface = function(name, methods) {
	    if (arguments.length != 2) {
	        throw new Error("Interface constructor called with " + arguments.length + "arguments, but expected exactly 2.");
	    }
	
	    this.name = name;
	    this.methods = [];
	    for (var i = 0, len = methods.length; i< len; i++) {
	        if (typeof methods[i] !== "string") {
	            throw new Error("Interface constructor expects method names to be passed in as a string.");
	        }
	        this.methods.push(methods[i]);
	    }
	};
	
	// インタフェースクラスのstaticメソッド (必要なメソッドが存在しているかチェックするメソッド)
	Interface.ensureImplements = function(object) {
	    if (arguments.length < 2) {
	        throw new Error("Function Interface.ensureImplements called with " + arguments.length + "arguments, but expected at least 2.");
	    }
	
	    for (var i = 1, len = arguments.length; i < len; i++) {
	        var interface = arguments[i];
	        // ensureImplementsの第二引数~がインタフェースクラスのオブジェクトでなければエラー
	        if (interface.constructor !== Interface) {
	            throw new Error("Function Interface.ensureImplements expects arguments two and above to be instances of Interface.");
	        }
	
	        // 各インタフェースクラスが定めるメソッドが存在しているかどうかのチェック
	        for (var j = 0, methodsLen = interface.methods.length; j < methodsLen; j++) {
	            var method = interface.methods[j];
	            if (!object[method] || typeof object[method] !== "function") {
	                throw new Error("Function Interface.ensureImplements: object does not implement the " + interface.name + " interface. Method " + method + " was not found.");
	            }
	        }
	    }
	};
	KTLab.Interface = Interface;
    
})(KTLab || (KTLab = {}));

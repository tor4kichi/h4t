var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var KTLab;
(function (KTLab) {
    var WeaponTimer = (function (_super) {
        __extends(WeaponTimer, _super);
        function WeaponTimer(id, repopTime, firstSubTime, loopSubTime) {
            var _this = this;
            _super.call(this, id, repopTime, function () {
                console.debug("WeaponTimer error!");
            });
            this.firstSubTime = 0;
            this.loopSubTime = loopSubTime;
            this.onEnd = null;
            this.timerLisName = "";
            this.State = WeaponTimer.notifyState_none;
            this.timerLisName = id + "TimeLis";
            this.firstSubTime = firstSubTime;
            this.State = WeaponTimer.notifyState_none;
            _super.prototype.SetStartCallback.call(this, function () {
                KTLab.Event.Queue("weapon-time-started", { time: _this.GetCurrentTime(), id: _this.GetId() });
            });
            _super.prototype.SetIntervalCallback.call(this, function (curtime) {
                KTLab.Event.Queue("weapon-time-interval", { time: curtime, id: _this.GetId() });
                if (curtime <= WeaponTimer.finalNotifyTime) {
                    if (_this.State != WeaponTimer.notifyState_final) {
                        console.debug("weapon final notify: id=" + _this.GetId());
                        KTLab.Event.Queue("weapon-time-finalnotify", { id: _this.GetId(), progress: 2 });
                    }
                    _this.State = WeaponTimer.notifyState_final;
                } else if (curtime <= WeaponTimer.secondNotifyTime) {
                    if (_this.State != WeaponTimer.notifyState_second) {
                        console.debug("weapon second notify: id=" + _this.GetId());
                        KTLab.Event.Queue("weapon-time-secondnotify", { id: _this.GetId(), progress: 1 });
                    }
                    _this.State = WeaponTimer.notifyState_second;
                } else if (curtime <= WeaponTimer.firstNotifyTime) {
                    if (_this.State != WeaponTimer.notifyState_first) {
                        console.debug("weapon first notify: id=" + _this.GetId());
                        KTLab.Event.Queue("weapon-time-firstnotify", { id: _this.GetId(), progress: 0 });
                    }
                    _this.State = WeaponTimer.notifyState_first;
                }
            });
            _super.prototype.SetEndCallback.call(this, function () {
                KTLab.Event.Queue("weapon-time-end", { id: _this.GetId(), progress:3 });
                _this.Start();
                _this.AddTime(_this.loopSubTime);
                _this.State = WeaponTimer.notifyState_none;
            });

            KTLab.Event.AddListener("game-start", this.timerLisName, this, function (e, that) {
            	that.Start();
            });

            KTLab.Event.AddListener("game-pause", this.timerLisName, this, function (e, that) {
            	if (that.IsRunning()) {
            		that.Pause();
            	}
            });

            KTLab.Event.AddListener("game-resume", this.timerLisName, this, function (e, that) {
           		that.Start();
            });
            KTLab.Event.AddListener("game-ended", this.timerLisName, this, function (e, that) {
                that.ResetTime();
                KTLab.Event.Queue("weapon-time-changed", { id: that.id, time: that.GetEndTime() });
            });
        }
        WeaponTimer.prototype.Release = function () {
            KTLab.Event.RemoveListenerFromName(this.timerLisName);
        };

        WeaponTimer.prototype.CraeteEventData = function () {
            return { id: this.GetId(), time: this.GetCurrentTime() };
        };

        WeaponTimer.prototype.GetFirstSubTime = function () {
            return this.firstSubTime;
        };
        
        WeaponTimer.prototype.GetLoopSubTime = function () {
            return this.loopSubTime;
        };
        WeaponTimer.prototype.ResetTime = function () {
            _super.prototype.ResetTime.call(this, _super.prototype.GetEndTime.call(this));
            this.State = WeaponTimer.notifyState_none;
        };
        WeaponTimer.notifyState_none = 0;
        WeaponTimer.notifyState_first = 1;
        WeaponTimer.notifyState_second = 2;
        WeaponTimer.notifyState_final = 3;

        WeaponTimer.firstNotifyTime = 30000;
        WeaponTimer.secondNotifyTime = 15000;
        WeaponTimer.finalNotifyTime = 10000;
        return WeaponTimer;
    })(KTLab.CountdownTimer);
    KTLab.WeaponTimer = WeaponTimer;
})(KTLab || (KTLab = {}));

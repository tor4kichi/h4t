var KTLab;
(function (KTLab) {
    var CountdownTimer = (function () {
        function CountdownTimer(TimerId, EndTime_ms, errorcallback) {
            this.Id = "";
            this.timerId = -1;
            this.workTime = 0;
            this.EndTime = 0;
            this.StartCallback = null;
            this.IntervalCallback = null;
            this.EndCallback = null;
            this.IsPause = false;
            this.Id = TimerId;
            this.timerId = -1;
            this.workTime = EndTime_ms;
            this.EndTime = EndTime_ms;
            this.StartCallback = null;
            this.IntervalCallback = null;
            this.EndCallback = null;
            this.IsPause = false;
        }
        CountdownTimer.prototype.SubTime = function (t) {
            this.workTime -= t;
        };

        CountdownTimer.prototype.AddTime = function (t) {
            this.workTime += t;
        };

        CountdownTimer.prototype.Stop = function () {
            if (this.timerId != -1) {
                console.debug("Timer:" + this.Id + " Stop.");
                clearInterval(this.timerId);
            }
            this.timerId = -1;
            this.IsPause = false;
        };

        CountdownTimer.prototype.Start = function () {
            var _this = this;
            if (this.IsPause == true) {
                this.IsPause = false;
            } else {
                this.workTime = this.EndTime;
                if (this.StartCallback != null) {
                    this.StartCallback();
                }
            }
            this.Stop();
            
            this.timerId = setInterval(function () {
                _this.workTime -= CountdownTimer.Interval;

                if (_this.workTime <= 0) {
                    _this.Stop();

                    if (_this.EndCallback != null) {
                        _this.EndCallback();
                    }
                } else if (_this.IntervalCallback != null) {
                    _this.IntervalCallback(_this.workTime);
                }
            }, 1000);
           

            console.debug("TimerStart: name=" + this.Id + ", endTime=" + this.workTime + ", TimerId="+ this.timerId);
        };

        CountdownTimer.prototype.GetCurrentTime = function () {
            return this.workTime;
        };

        CountdownTimer.prototype.GetEndTime = function () {
            return this.EndTime;
        };

        CountdownTimer.prototype.Pause = function () {
            this.Stop();
            this.IsPause = true;
        };

        CountdownTimer.prototype.IsRunning = function () {
            return this.timerId != -1 || this.IsPause;
        };

        CountdownTimer.prototype.ResetTime = function (Time) {
            if (this.IsRunning()) {
                console.debug("CountdownTimer: Running in call ResetTime(). TimerId=" + this.Id);
            }

            if (Time != undefined) {
                this.EndTime = Time;
            }
            this.workTime = this.EndTime;
        };

        CountdownTimer.prototype.SetIntervalCallback = function (callback) {
            this.IntervalCallback = callback;
        };

        CountdownTimer.prototype.SetStartCallback = function (callback) {
            this.StartCallback = callback;
        };

        CountdownTimer.prototype.SetEndCallback = function (callback) {
            this.EndCallback = callback;
        };

        CountdownTimer.prototype.GetId = function () {
            return this.Id;
        };
        CountdownTimer.Interval = 1000;
        return CountdownTimer;
    })();
    KTLab.CountdownTimer = CountdownTimer;
})(KTLab || (KTLab = {}));

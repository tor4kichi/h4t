var App;
(function (App) {
    function RegistVibrateEventListener(vibrateFunc) {
    }
    App.RegistVibrateEventListener = RegistVibrateEventListener;

    var VibrateManager = (function () {
        function VibrateManager(shortVibTime, midVibTime, longVibTime) {
            this.ShortVibTime = 50;
            this.MiddleVibTime = 100;
            this.LongVibTime = 200;
            this.ShortVibTime = shortVibTime;
            this.MiddleVibTime = midVibTime;
            this.LongVibTime = longVibTime;

            KTLab.Event.AddListener("vib-short", "appvibrate", this, function (e, that) {
                KTLab.Event.Queue("vibration", { time: that.ShortVibTime });
            });
            KTLab.Event.AddListener("vib-middle", "appvibrate", this, function (e, that) {
                KTLab.Event.Queue("vibration", { time: that.MiddleVibTime });
            });
            KTLab.Event.AddListener("vib-long", "appvibrate", this, function (e, that) {
                KTLab.Event.Queue("vibration", { time: that.LongVibTime });
            });
        }
        return VibrateManager;
    })();
    App.VibrateManager = VibrateManager;
})(App || (App = {}));


var CONST;
(function (CONST) {
	var SOUND = {};

	SOUND.names = ["buzzer", "zephyr", "yukkuri"];

	SOUND.types =
	[
	  {name:SOUND.names[0], text:"電子音", subtext:"lentasound.com", testSound:["sr"]},
	  {name:SOUND.names[1], text:"男性ボイス ver.ZEPHYR", subtext:"twitter@zephyritself", testSound:["snapshot"]},
	  {name:SOUND.names[2], text:"ゆっくりボイス", subtext:"softalk", testSound:["snapshot"]},
	];

	CONST.SOUND = SOUND;


	var WEAPON = {};

	WEAPON.names = ["roke", "oursr", "os", "theresr"];

	WEAPON.infos =
	[
	 	{name:WEAPON.names[0], text:"ロケラン", textlong:"ロケットランチャー"},
	 	{name:WEAPON.names[1], text:"スナ１", textlong:"スナイパーライフル１"},
	 	{name:WEAPON.names[2], text:"オバシ", textlong:"オーバーシールド"},
	 	{name:WEAPON.names[3], text:"スナ２", textlong:"スナイパーライフル２"},
	];

	CONST.WEAPON = WEAPON;

	CONST.mobiletest = false;

})(CONST || (CONST = {}));
 console.debug("dialog js");
$(document).on("pageinit", "#setting-dialog", function(){
 
	 console.debug("setting main pageinit");
	 $("#select-countdown").change(function(){
	   var op = $.cookie("opt");
	   var coundDownEnable = $("#select-countdown").val() == "yes" ? true : false;
	   op.cdenable = coundDownEnable;
	   $.cookie("opt", op);
	   
	   if(coundDownEnable) {
		   $("#settingdlg-div-li-cdprio-prio").show();
	   } else {
		   $("#settingdlg-div-li-cdprio-prio").hide();
	   }
	   $("#select-countdown").slider("refresh");
   });
	 
	 var dialogRefresh = function() {
		// ダイアログを呼び出したとき毎回呼び出される
		   
		   // クッキーを取得して、セレクトメニューの初期状態設定を行う
		   var opt = $.cookie("opt");
		   console.debug(opt.sound_pattern);
		   
		   for(var i in CONST.SOUND.types) {
			   if(opt.sound_pattern == CONST.SOUND.types[i].name) {
				   $("#soundtype-disp").text("");
				   $("#soundtype-disp").text(CONST.SOUND.types[i].text);
				   break;
			   }
		   }
		/*	   if(opt.sound_pattern == "buzzer") {
				   $("#soundtype-disp").text("電子音");
			   }else if(opt.sound_pattern == "zephyr") {
				   $("#soundtype-disp").text("男性セコンドボイス");
			   }
		*/	   
		   
		
		   var initcoundDownEnable = opt.cdenable;
		   console.debug(initcoundDownEnable);
		   if(initcoundDownEnable) {
			   $("#settingdlg-div-li-cdprio-prio").show();
		   } else {
			   $("#settingdlg-div-li-cdprio-prio").hide();
		   }
		   $("#select-countdown").val(initcoundDownEnable == true ? "yes" : "no");
		   $("#select-countdown").slider("refresh");
		   
		   
		   
		   // 優先順位のテキストを作成する
		   var tmpldata = {prio:[]};
		   for(var i = opt.prio.length-1; i >= 0; --i) {
			   for(var m = CONST.WEAPON.infos.length-1; m >= 0; --m) {
				   if(CONST.WEAPON.names[m] == opt.prio[i]) {
					   
					   var text = CONST.WEAPON.infos[m].textlong;
					   tmpldata.prio.unshift(text);
					   break;
				   }
			   }
		   }
		   
		   $("#prio-text").empty();
		   $("#prio-text").append($("#ol-li-cdprio-tmpl").tmpl(tmpldata));
		   
	 };
	 
 
	 $("#setting-dialog").on("pagebeforeshow", function(){
	   console.debug("dialog pageshow");
	   dialogRefresh();
	 });
	 
	 $("#setting-dlg-reset").click(function(){
		 $("#setting-dialog ul:first").fadeTo(250, 0.5);
		 
		 setTimeout(function(){
			 KTLab.Event.Trigger("ui-click-allreset");
			 dialogRefresh();
			 $.mobile.loading("show");
			 
			 setTimeout(function(){
				 $("#setting-dialog ul:first").fadeTo(250, 1.0);
				 $.mobile.loading("hide");
			 }, 500);
		 }, 250);
	 });
	 
	 
	 
	 $("#setting-dialog").on("pagebeforehide", function(event){
	      // 
		 console.debug("dlg ok btn");
		 var op = $.cookie("opt");
		   
//	 opt.sound_pattern = $("#select-soundtype").val();
		   
		 op.cdenable = $("#select-countdown").val() == "yes" ? true : false;
		   
		 $.cookie("opt", op);
		 
		 console.debug("cdenable:"+ op.cdenable);
     });
});
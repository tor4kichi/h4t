
var h4t;
(function (h4t) {
	
	
    var soundBaseAPI = function(type, soundNameAry){
    	
		this.__type = type;
		this.__sounds = {};
		this.__needSoundNames = soundNameAry;
		
		for(var i in this.__needSoundNames) {
	    	this.__Load(this.__needSoundNames[i]);
		}
	};
	
	soundBaseAPI.IsMobile = false;
	
	soundBaseAPI.prototype = {
		
		__Play : function(name){
			if(name in this.__sounds) {
				this.__sounds[name].play();
			}
			else 
			{
				throw "読み込まれていない音はplay出来ません。:"+name;
			}
		},
		
		AllStop : function() {
			for(var i in this.__sounds) {
				this.__sounds[i].stop();
			}
		},
		
		AllPause : function() {
			for(var i in this.__sounds) {
				this.__sounds[i].pause();
			}
		},
		
		AllResume : function() {
			for(var i in this.__sounds) {
				this.__sounds[i].resume();
			}
		},
		
		__initialized : false,
		
		__Load : function(name) {
			var platform = soundBaseAPI.IsMobile == true ? "mobile" : "pc";
			var withoutExt = "sound/"+platform+"/"+this.__type+"/"+name;
			console.debug(withoutExt);
			this.__sounds[name] = new KTLab.sound(withoutExt+".ogg|"+withoutExt+".mp3");
		},
		
		Release : function() {
			for(var i in this.sounds){
	    		this.sounds[i].release();
	    	}
		},
	};
	
	h4t.soundBaseAPI = soundBaseAPI;
	
	var soundPlayer_PC = function(type){
    	
		this.__type = type;
		this.__sounds = {};
		$.ajax({
			context:this, 
			url:"./sound/"+type+"/all.json",
			dataType: "json",
			success : function(data){
				for(i in data.preset) {
					this.__sounds[i] = this.__load(this.__type, i);
				}
			},
			async:false,
		});
		
	};
	
	soundPlayer_PC.prototype = {
		
		play : function(name){
			if(name in this.__sounds) {
				this.__sounds[name].play();
			}
			else 
			{
				throw "読み込まれていない音はplay出来ません。:"+name;
			}
		},
		
		stop : function() {
			for(var i in this.__sounds) {
				this.__sounds[i].stop();
			}
		},
		
		pause : function() {
			for(var i in this.__sounds) {
				console.debug(i);
				this.__sounds[i].pause();
			}
		},
		
		resume : function() {
			for(var i in this.__sounds) {
				this.__sounds[i].resume();
			}
		},
		
		__load : function(type, name) {
			var withoutExt = "sound/"+type+"/"+name;
			console.debug(withoutExt);
			return new KTLab.sound(withoutExt+".ogg|"+withoutExt+".mp3");
		},
		
		release : function() {
			for(var i in this.__sounds){
	    		this.__sounds[i].release();
	    	}
		},
	};
	
	h4t.soundPlayer_PC = soundPlayer_PC;
	
	var soundPlayer_MOBILE = function(type){
    	
		this.__player = null;
		this.__soundmap = null;
		$.ajax({
			context: this,
			url:"./sound/"+type+"/all.json",
			dataType: "json",
			success : function(data){
				
				var _this = this;
				_this.__soundmap = data;
				console.debug(data);
				if(UnitePlayer.ios != 0 || UnitePlayer.android != 0) {
					$("html").one("click", function() {
						_this.__player = new UnitePlayer(data, 0, soundPlayer_MOBILE.Callback);
						_this.__player.preset("silence");
					});
				} else {
					_this.__player = new UnitePlayer(data, 0, soundPlayer_MOBILE.Callback);
				}
			},
			async:false,
		});
		
		
		console.debug(this.__player);
	};
	soundPlayer_MOBILE.canplay = false;
	
	soundPlayer_MOBILE.Callback = function(audioEventObject,  // @param Event: Audio Event オブジェクト
				unitePlayerObject, // @param Instance: UnitePlayerのインスタンス
				trackNumber,       // @param Number: UnitePlayer生成時に指定したトラック番号
				currentTime) {     // @param Number: 現在の再生ヘッドの位置(再生/一時停止中の位置)
		switch (audioEventObject.type) {
			  case "loadstart":   // 読み込み開始
				  KTLab.Util.toast.show("音声の読み込みを開始", 1500);
				  soundPlayer_MOBILE.canplay = false;
				  break;
			  case "canplay":     // 再生可能
				  KTLab.Util.toast.show("音声の再生準備完了", 1500);
				  soundPlayer_MOBILE.canplay = true;
				  break;
			  case "load":        // 読み込み完了
				  KTLab.Util.toast.show("音声の読み込み完了", 1500);
				  soundPlayer_MOBILE.canplay = true;
				  
				  break;
			  case "abort":       // 読み込み中断
				  KTLab.Util.toast.show("音声の読み込み中断", 5000);
				  break;
			  case "error":       // エラー
				  KTLab.Util.toast.show("この端末はブラウザでの音声再生に対応していません。", 5000);
				  break;
			  case "durationchange": // 読み込みにより再生可能時間が変化(延長)
				  break;
			  case "playing":     // 再生開始
				  break;
			  case "pause":       // 一時停止
				  break;
			  case "ended":       // 再生終了し一時停止
				  break;
		  };
		  
		  console.debug(audioEventObject);
	};	
	
	soundPlayer_MOBILE.prototype = {
		
		play : function(name){
			if(soundPlayer_MOBILE.canplay) {
				this.__player.preset(name);
				console.debug("再生："+name);
			} else {
				console.debug("未再生："+name);
			  KTLab.Util.toast.show("音声準備中・・・。", 1500);

			}
		},
		
		stop : function() {
			this.__player.stop();
		},
		
		pause : function() {
			this.__player.pause();
		},
		
		resume : function() {
			var info = this.__player.info();
			if(info.paused == true) {
				this.__player.pause();
			}
		},
		
		release : function() {
			
		},
	};
	h4t.soundPlayer_MOBILE = soundPlayer_MOBILE;
	
})(h4t || (h4t = {}));

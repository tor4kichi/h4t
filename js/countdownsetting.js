$(document).on("pageinit", "#countdownprio-dialog", function(){

	var Save
	var dlg = $("#countdownprio-dialog");
	console.debug("countdownprio pageinit");
	dlg.on("pagebeforeshow", function(){
		 
		// 優先順位リストの初期化
	   // opt.prio == cd_li_item[n].valなアイテムをcd_li_itemの先頭に持ってくる
	   // この時opt.prioを逆順で回すことで正しい並び順で取り出す
	   var cd_li_item = [
    		 {
    			 dispname:"↓対象外↓",
    			 val:"__",
    			 themeid:"a",
    		 },
    		 {
    			 dispname:"ロケットランチャー",
    			 val:"roke",
    			 themeid:"d",
    		 },
    		 {
    			 dispname:"オーバーシールド",
    			 val:"os",
    			 themeid:"d",
    		 },
    		 {
    			 dispname:"スナイパーライフル(▲)",
    			 val:"oursr",
    			 themeid:"d",
    		 },
    		 {
    			 dispname:"スナイパーライフル(▼)",
    			 val:"theresr",
    			 themeid:"d",
    		 },
    	];
	     var opt = $.cookie("opt");
		 var cd_enableitems = opt.prio;
		 for(var i = cd_enableitems.length; i >= 0; --i ) {
			 var saved = cd_enableitems[i];
			 for(var m in cd_li_item) {
				 if(saved == cd_li_item[m].val) {
					var s = cd_li_item.splice(m, 1);
					cd_li_item.unshift(s[0]);
				 }
			 }
		 }
		 console.debug("入れ替えチェック");
		 for(var i in cd_li_item) {
			 console.debug(cd_li_item[i].val);
		 }
         var priolist = $("#countdownprio-dialog #cd-prio-list");
		 priolist.empty();
		 var liary = $("#cd-prio-li-tmpl").tmpl(cd_li_item);
		 liary.each(function(){
			 
			 
			 var whereIndex = function(data) {
				 var val = $(data).attr("value");
				 var pl = $("#cd-prio-list");
				 
				 var rtn = -1;
				 pl.find("li").each(function(idx, that){
					 if(val == $(that).attr("value")) {
						 rtn = idx;
						 return false;
					 }					 
				 });
				 return rtn;
			 };
			 
			 $(this).find("button").each(function(){$(this).button();});
			 $(this).find("#prio-up-btn").click(function(){
				 var idx = whereIndex(this) + 1; // ヘッダの分 ＋１する
				 if(idx != 0) {
					 var li = $("ul#cd-prio-list li:nth-child("+idx+")");
					 var prev = li.prev();
					 li.after(prev);
					 
					 $("#cd-prio-list").listview("refresh");
				 }
			 });
			 $(this).find("#prio-down-btn").click(function(){
				 var idx = whereIndex(this) + 1; // ヘッダの分 ＋１する
				 console.debug($("#cd-prio-list li").length);
				 if(idx <= $("#cd-prio-list li").length) {
					 var li = $("ul#cd-prio-list li:nth-child("+idx+")");
					 var next = li.next();
					 next.after(li);
					 
					 $("#cd-prio-list").listview("refresh");
				 }				 
			 });
		 });
		 priolist.append(liary);
		 priolist.listview("refresh");
	 });
	 
	dlg.on("pagebeforehide", function(event){
	      // 
		 console.debug("dlg ok btn");
		 var opt = $.cookie("opt");
		 
		 var outitem = [];
		   
		 
		 dlg.find("ul li").each(function(e, that){
			 var val = $(that).attr("value");
			 if(val == "__") {
				 return false;
			 }
			 outitem.push(val);
		 });
		 
		 console.debug(outitem);
		 
		 
		 opt.prio = outitem;
		   
		 $.cookie("opt", opt);
    });
 });
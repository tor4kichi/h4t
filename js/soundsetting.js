
$(document).on("pageinit", "#sound-choice-dialog", function(){
 
	console.debug("sound setting dlg pageinit");
	
	var ismobile = createjs.Sound.BrowserDetect.isIOS || createjs.Sound.BrowserDetect.isAndroid;
	var mobileTesting = false;
	
	var testSeApi;
	if(ismobile || CONST.mobiletest) {
		testSeApi = new h4t.soundPlayer_MOBILE("test");
		console.debug("test mobile");
	} else {
		testSeApi = new h4t.soundPlayer_PC("test");
		console.debug("test pc");
	}
	
	$("#sound-choice-dialog").on("pagebeforeshow", function(){
		
		
		var op = $.cookie("opt");
		console.debug(op);
		var tmpData = [];
		$.extend(true, tmpData, CONST.SOUND.types);
		
		console.debug("通知音:" + op.sound_pattern);
		for(var i in tmpData) {
			console.debug(tmpData[i].name);
			if(op.sound_pattern == tmpData[i].name) {
				tmpData[i].themeid ="e";
			} else {
				tmpData[i].themeid ="d";
			}
		}
		var li = $("#soundtype-li-tmpl").tmpl(tmpData);
		
		li.each(function(idx, val) {
			var that = $(this);
			that.find(".soundtype-text").bind("click", that.attr("value"), function(e) {
				var opt = $.cookie("opt");
				console.debug(e.data);
				opt.sound_pattern = e.data;
				$.cookie("opt", opt);
				console.debug(op);
			});
			that.find(".soundtype-testplay").bind("click", tmpData[idx].name, function(e){
				testSeApi.play(e.data);
			});
			
		});
		var ul = $("#dlg-ul-sound-list");
		ul.empty();
		ul.append(li);
		ul.listview("refresh");
		
	});
});
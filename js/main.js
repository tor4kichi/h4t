

	var options = {};	// クッキーに保存する/されるオプション
	var appWinW = 380;	// 幅、小画面で開いたときのウィンドウサイズ
	var appWinH = 520;  // 高さ、〃


	// オプション保存
	var SaveOptions = function() {
		$.cookie("opt", options);
		console.debug("optionを保存しました。");
		console.debug(options);

	};


	// オプション項目をデフォルト値にリセット＆保存
	var ResetOptions = function() {
		options = {
				gameTime: 15,
				readyTime: 5,
				weapons:
					{
					roke   :{icon:0, enable:true,  time:180000, firstsubtime:0, loopsubtime:0},
					oursr  :{icon:1, enable:true,  time:120000, firstsubtime:5, loopsubtime:1},
					os     :{icon:2, enable:true,  time:120000, firstsubtime:5, loopsubtime:1},
					theresr:{icon:1, enable:false, time:120000, firstsubtime:5, loopsubtime:1},
					},
				sound_pattern: "zephyr", // "zephyr"
				cdenable: true,			// カウントダウンの有効フラグ
				prio:["roke", "os"],    //
			};
		SaveOptions();
	};

	// オプションからカウントダウンする項目を取得します。
	// カウントダウンしない場合orカウントダウンする対象がない場合はnullを返す
	var GetCountdownItemFromOptions = function() {
		if(options.cdenable == false) {
			return null;
		}
		var len = options.prio.length;
		for(var i = 0; i < len; ++i) {
			if(options.weapons[options.prio[i]].enable) {
				return options.prio[i];
			}
		}
		return null;
	};

	// オプション項目を読み込みます
	var ReadOptions = function() {
		return $.cookie("opt");
	};

	// 内部設定をデフォルトにセットアップします。
	var SetupDefault = function()
	{
		KTLab.Event.Trigger("weapon-reset", null);

		for(var i in options.weapons) {
			var w = options.weapons[i];
			KTLab.Event.Trigger("weapon-create",
		    		{id:i, iconid:w.icon, repoptime:w.time, firstsubtime:w.firstsubtime, loopsubtime:w.loopsubtime });
			if(w.enable == false) {
			    KTLab.Event.Trigger("weapon-toggle-enable", {id:i});
			}
		}

	    console.debug("readyTime:" + options.readyTime);
	    $("#select-choice-gametime").val(options.gameTime);
	    $("#select-choice-gametime").selectmenu("refresh");
		$("#select-choice-readytime").val(options.readyTime);
	    $("#select-choice-readytime").selectmenu("refresh");
		KTLab.Event.Queue("game-time-reset", {time: KTLab.MinuteToMS(options.gameTime)});
		KTLab.Event.Queue("game-ready-time-reset", {time: options.readyTime * 1000});
	};

	// 表示状態をデフォルト表示に
	var resetDisplay = function() {
		// スピナーを表示
		setTimeout(function(){
	        $("#gametime-process").css('display', 'none');
	        $("#gametime-select").css('display', 'inline-block');
	        console.debug("select-choice-gametime selectmenu prev");
	        $("#select-choice-gametime").selectmenu('enable');
		}, 1000);

		$("#startBtn").fadeIn(300);
	    $("#nowBtn").fadeIn(300);

//		$("#startBtn").css('display', 'inline-block');
//	    $("#nowBtn").css('display', 'inline-block');
	    $("#resumeBtn").css('display', 'none');
	    $("#pauseBtn").css('display', 'none');
	    $("#resetBtn").css('display', 'none');

		var t = parseInt($("#select-choice-gametime").val()); // UIから時間の初期値を取得
			t = KTLab.MinuteToMS(t);
		KTLab.Event.Queue("game-time-reset", {time:t});
	};

	var applisName = "appLis";
	var sePlay = function(type) {
		if(player == null || player == undefined) {
			return;
		}

		player.play(type);
    };
    var player = null;

    // ページ初期化時の処理
	$(document).on("pageinit", "#mainpage", function(){

		if(CONST.mobiletest) {
			KTLab.Util.toast.show("デバッグ：モバイルテストが有効になっています。", 1500);
		}

		$.cookie.json = true;
		$.cookie.defaults.path = '/';

		options = ReadOptions();
		console.debug("saved option");
		console.debug(options);
		if(options == undefined || options.length == 0) {
			ResetOptions();
			KTLab.Util.toast.show("H4Timerにようこそ！", 5000);
			KTLab.Util.toast.show("右上の歯車ボタンから音声切り替えが出来るよ:)", 5000);
			if(UnitePlayer.ios != 0) {
				KTLab.Util.toast.show("iOSでは画面表示のために電源に接続することをオススメしています", 5000);
			}
		}
		SetupDefault();

		// タイマー開始ボタンの文字列を更新する関数オブジェクト
		var StartNowBtnUpdate = function() {
	    	var gametimeMS = KTLab.MinuteToMS(parseInt($("#select-choice-gametime").val())); // UIから時間の初期値を取得
	    	var countdownMS = parseInt($("#select-choice-readytime").val()) * 1000;
	    	$("#nowBtn-time").text(KTLab.MStoTimeString(gametimeMS));
	    	$("#startBtn-time").text(KTLab.MStoTimeString(gametimeMS + countdownMS));

		}
		StartNowBtnUpdate();

		// 試合時間、試合予備時間の変更にイベントをバインドする。
	    $("#select-choice-gametime").change(function(that){
	    	var gametime = parseInt($("#select-choice-gametime").val()); // UIから時間の初期値を取得
			options.gameTime = gametime;
			SaveOptions();
	    	KTLab.Event.Queue("game-time-reset", {time:KTLab.MinuteToMS(gametime)});

	    	StartNowBtnUpdate();
	    });
	    $("#select-choice-readytime").change(function(that){
	    	var countdown = parseInt($("#select-choice-readytime").val());
	    	console.debug("select-readyTime:"+countdown);
	    	KTLab.Event.Queue("game-ready-time-reset", {time:countdown*1000});
//			t = KTLab.MinuteToMS(t);
			options.readyTime = countdown;
			SaveOptions();

			StartNowBtnUpdate();
		});

		var SetBtnAction = function(name)
	    {
	    	$("#"+name+"Btn").click({}, function(e){
				KTLab.Event.Queue('ui-click-'+ name, null);
				e.preventDefault();
			});
	    };
	    var SetBtnActionWithDafaultEvent = function(name)
	    {
	    	$("#"+name+"Btn").click({}, function(e){
				KTLab.Event.Queue('ui-click-'+ name, null);
			});
	    };

	    //
	    SetBtnAction("now");
	    SetBtnAction("start");
	    SetBtnAction("pause");
	    SetBtnAction("resume");
	    SetBtnAction("allreset");
	    SetBtnActionWithDafaultEvent("setting");

	    var StartEventProc = function() {
			setInterval(function()
			{
				try {
					KTLab.Event.EM.Process();
				} catch(e) {
					KTLab.Util.toast.show(""+e, 5000);
					setTimeout(function(){
						StartEventProc();
					}, 500);

				}
			}, 100);
	    };

	    StartEventProc();

		if(UnitePlayer.ios != 0 || UnitePlayer.android != 0 || CONST.mobiletest) {
			// モバイルでの動作
			KTLab.Util.toast.show("画面をタップしてください。音の準備が始まります。", 7000);
		} else {
			// PCでの動作
			KTLab.sound.Init();	// サウンド初期化

			$("#onlydesktop").show();
			var newWin = null;
		    $("#onlydesktop").click(function(e){
		    	e.preventDefault();
		    	if(newWin != null && !newWin.closed)
		    	{
		    		newWin.focus();
		    	}
		    	else
		    	{
		    		newWin = window.open(window.location.href, 'H4Timer',
		    			'width='+appWinW+',height='+appWinH+',scrollbars=no,status=no,toolbar=no,location=no,menubar=no,resizable=no,top=100'
		    		);
		    		newWin.focus();
		    	}
			});
		}

		// タイトルをクリックでページのリロード
		var captionClickCount = 0;
		$("#caption-text").click({cnt:captionClickCount}, function(e){
			var cnt = e.data.cnt;
			if(cnt == 1) {
				window.location.reload();
			} else {
				KTLab.Util.toast.show("もう一度クリックすると再読み込みします（注：動作中タイマーはリセットされます）", 3000);
				setTimeout(function(e){
					--e.data.cnt;
					KTLab.Util.toast.show("カウントリセット", 500);
				}, 3000, e);
			}
			++e.data.cnt;
		});


	    fastClick(); // .fastclick
	    
	    $("#mainpage").css("display","");
	});

	// ページを表示する直前
	// 音再生の初期化とウェポンリストの表示リセット
	$("#mainpage").on("pagebeforeshow", function(){
		options = ReadOptions();
		console.debug(options);
		console.debug("#mainpage before show");

		// 環境ごとにplayerの実装を選択
		if(UnitePlayer.ios != 0 || UnitePlayer.android != 0 || CONST.mobiletest) {
	    	player = new h4t.soundPlayer_MOBILE(options.sound_pattern);
	    	console.debug("音楽プレイヤーを モバイルモード で生成");
		} else {
			player = new h4t.soundPlayer_PC(options.sound_pattern);
			console.debug("音楽プレイヤーを PCモード で生成");
		}

		if(options.sound_pattern == "buzzer") {
	    	SEAPI = buzzerSeAPI;
			KTLab.Util.toast.show("現在の通知音 "+ CONST.SOUND.types[0].text, 3000);
	    } else if(options.sound_pattern == "zephyr") {
		    SEAPI = zephyrSeAPI;
			KTLab.Util.toast.show("現在の通知音 "+ CONST.SOUND.types[1].text, 3000);
	    } else if(options.sound_pattern == "yukkuri") {
	    	SEAPI = yukkuriSeAPI;
	    	KTLab.Util.toast.show("現在の通知音 "+ CONST.SOUND.types[2].text, 3000);
	    }
		console.debug(options.sound_pattern);

		weaponLv.SetCountdownDisplay();
	});


	// 別ページへ遷移時
	$("#mainpage").on("pagehide", function(){
		console.debug("#mainpage hide");
		if(player != null) {
			player.release();
			delete player;
			player = null;
		}
	});


	// イベントタイプの登録
	try
	{
		App.EventTypeRegist();
	    EventImpl.EventTypeRegist();
	}
	catch(e)
	{
		console.debug(e.message);
	}


	var gameTimer = new KTLab.H4Timer.GameTimerInfo();
	var weaponLv = new KTLab.WeaponListview("#timer-list");

	// 通知をチェインプロセスさせる how to use: Add(time, func, item)
	var notifyMan = new KTLab.ChainProcessManager();
	notifyMan.Start(32 /*監視間隔 ms*/);

	var sound_delay = 500;


	// 電子音の鳴らし方を定義
	var buzzerSeAPI = function() {};
	buzzerSeAPI.ReadyStart = function(time) {
    	player.play("ready-start");
    };

    buzzerSeAPI.Ready = function(time) {
    	player.play("ready");
    };
    buzzerSeAPI.Start = function() {
    	player.play("start");
    };

    buzzerSeAPI.Pause = function() {
		notifyMan.Add(sound_delay, function(that){
			player.play("pause");
		}, this);
    };

    buzzerSeAPI.Resume = function() {
		notifyMan.Add(sound_delay, function(that){
	    	player.play("resume");
		}, this);
    };

    buzzerSeAPI.End = function() {
		notifyMan.Add(sound_delay, function(that){
	    	player.play("end");
		}, this);
    };

    buzzerSeAPI.RokeFirst = function() {
		notifyMan.Add(sound_delay, function(that){
	    	player.play("roke");
		}, this);
    };

    buzzerSeAPI.RokeSecond = function() {
    	notifyMan.Add(sound_delay, function(that){
	    	player.play("roke");
		}, this);
    };

    buzzerSeAPI.RokeFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "roke") {
    		notifyMan.Add(1000, function(that){
    	    	player.play("roke");
    		}, this);
    		for(var i = 0; i < 8; ++i) {
        		notifyMan.Add(1000, function(that){
        	    	player.play("ready");
        		}, this);
    		}
    		notifyMan.Add(sound_delay, function(that){
    			notifyMan.Clear();
    		}, this);
    	} else {
    		notifyMan.Add(sound_delay, function(that){
    	    	player.play("roke");
    		}, this);
    	}
    };
    buzzerSeAPI.RokeRespawn = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "roke") {
    		notifyMan.Clear();
    	} else {
    	}
		notifyMan.Add(sound_delay, function(that){
	    	player.play("roke");
		}, this);

    };
    buzzerSeAPI.SRFirst = function() {
		notifyMan.Add(sound_delay, function(that){
	    	player.play("sr");
		}, this);
    };

    buzzerSeAPI.SRSecond = function() {
		notifyMan.Add(sound_delay, function(that){
	    	player.play("sr");
		}, this);
    };

    buzzerSeAPI.SRFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "oursr" || countdownItemName == "theresr") {
    		notifyMan.Add(1000, function(that){
    	    	player.play("sr");
    		}, this);
    		for(var i = 0; i < 8; ++i) {
        		notifyMan.Add(1000, function(that){
        	    	player.play("ready");
        		}, this);
    		}
    		notifyMan.Add(sound_delay, function(that){
    			notifyMan.Clear();
    		}, this);
    	} else {
    		notifyMan.Add(sound_delay, function(that){
    	    	player.play("sr");
    		}, this);
    	}
    };
    buzzerSeAPI.SRRespawn = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "oursr" || countdownItemName == "theresr") {
    		notifyMan.Clear();
    	} else {
    	}
		notifyMan.Add(sound_delay, function(that){
	    	player.play("sr");
		}, this);

    };
    buzzerSeAPI.OSFirst = function() {
		notifyMan.Add(sound_delay, function(that){
	    	player.play("os");
		}, this);
    };

    buzzerSeAPI.OSSecond = function() {
		notifyMan.Add(sound_delay, function(that){
	    	player.play("os");
		}, this);
    };

    buzzerSeAPI.OSFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "os") {
    		notifyMan.Add(1000, function(that){
    	    	player.play("os");
    		}, this);
    		for(var i = 0; i < 8; ++i) {
        		notifyMan.Add(1000, function(that){
        	    	player.play("ready");
        		}, this);
    		}
    		notifyMan.Add(sound_delay, function(that){
    			notifyMan.Clear();
    		}, this);
    	} else {
    		notifyMan.Add(sound_delay, function(that){
    	    	player.play("os");
    		}, this);
    	}
    };
	buzzerSeAPI.OSRespawn = function() {
		var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "os") {
    		notifyMan.Clear();
    	} else {
    	}
		notifyMan.Add(sound_delay, function(that){
	    	player.play("os");
		}, this);
    };

	// セコンドボイス（男性/Zepherさん）の鳴らし方を定義
	var zephyrSeAPI = function() {};

	//
	zephyrSeAPI.__Delay = {
	    	_15: 500,
	    	_20: 300,
	    	_30: 250,
	    	_roke: 1500+0, //
	    	_sr : 1600+0,  // 〃
	    	_os : 1600+0,  // 〃
	    	_countdown: 9000,
	    	_secondsremaining: 1150,
	    	_gameover: 1000, //
	    	_snapshot: 1000,

	    	other: 300,
	    };

	    zephyrSeAPI.DelayPlay = function(name) {
	    	var t = "_" + name;
	    	var d = t in zephyrSeAPI.__Delay ? zephyrSeAPI.__Delay[t] : zephyrSeAPI.__Delay["other"];
	    	notifyMan.Add(d, function(that){
	    		player.play(name);
			}, this);
	    };

	zephyrSeAPI.ReadyStart = function(time) {
    	player.play("ready-start");
    };

    zephyrSeAPI.Ready = function(time) {
    	player.play("ready");
    };
    zephyrSeAPI.Start = function() {
    	player.play("start");
    };

    zephyrSeAPI.Pause = function() {
//    	player.play("pause");
    };

    zephyrSeAPI.Resume = function() {
//    	player.play("resume");
    };

    zephyrSeAPI.End = function() {
    	player.play("gameover");
    };

    zephyrSeAPI.RokeFirst = function() {
    	zephyrSeAPI.DelayPlay("30");
    	zephyrSeAPI.DelayPlay("secondsremaining");
    	zephyrSeAPI.DelayPlay("roke");
    };

    zephyrSeAPI.RokeSecond = function() {
    	zephyrSeAPI.DelayPlay("15");
    	zephyrSeAPI.DelayPlay("secondsremaining");
    	zephyrSeAPI.DelayPlay("roke");
    };

    zephyrSeAPI.RokeFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "roke") {
	    	zephyrSeAPI.DelayPlay("countdown");
    	} else {
    		// 残り10秒だ
    	}
    };
    zephyrSeAPI.RokeRespawn = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "roke") {
    		notifyMan.Clear();
    		zephyrSeAPI.DelayPlay("roke");
    	} else {
    		zephyrSeAPI.DelayPlay("roke");
    	}

    };
    zephyrSeAPI.SRFirst = function() {
    	zephyrSeAPI.DelayPlay("30");
    	zephyrSeAPI.DelayPlay("secondsremaining");
    	zephyrSeAPI.DelayPlay("sr");
    };

    zephyrSeAPI.SRSecond = function() {
    	zephyrSeAPI.DelayPlay("15");
    	zephyrSeAPI.DelayPlay("secondsremaining");
    	zephyrSeAPI.DelayPlay("sr");
    };

    zephyrSeAPI.SRFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "oursr" || countdownItemName == "theresr") {
	    	zephyrSeAPI.DelayPlay("countdown");
    	} else {
    		// 残り10秒だ
    	}
    };
    zephyrSeAPI.SRRespawn = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "oursr" || countdownItemName == "theresr") {
    		notifyMan.Clear();
	    	zephyrSeAPI.DelayPlay("sr");
    	} else {
	    	zephyrSeAPI.DelayPlay("sr");
    	}

    };
    zephyrSeAPI.OSFirst = function() {
    	zephyrSeAPI.DelayPlay("30");
    	zephyrSeAPI.DelayPlay("secondsremaining");
    	zephyrSeAPI.DelayPlay("os");
    };

    zephyrSeAPI.OSSecond = function() {
    	zephyrSeAPI.DelayPlay("15");
    	zephyrSeAPI.DelayPlay("secondsremaining");
    	zephyrSeAPI.DelayPlay("os");
    };

    zephyrSeAPI.OSFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "os") {
	    	zephyrSeAPI.DelayPlay("countdown");
    	} else {
    		// 残り10秒だ
    	}
    };
    zephyrSeAPI.OSRespawn = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "os") {
    		notifyMan.Clear();
	    	zephyrSeAPI.DelayPlay("os");
    	} else {
    		zephyrSeAPI.DelayPlay("os");
    	}
    };

    var yukkuriSeAPI = function() {};

	//
	yukkuriSeAPI.__Delay = {
			_5: 600,
	    	_15: 800,
	    	_20: 800,
	    	_30: 1000,
	    	_start:1000,
	    	_gameover: 1000, //
	    	_madeato: 600,
	    	_roke: 700+0, //
	    	_sr : 700+0,  // 〃
	    	_os : 600+0,  // 〃
	    	_countdown: 9000,
	    	_snapshot: 1200,

	    	other: 300,
	    };

	    yukkuriSeAPI.DelayPlay = function(name) {
	    	var t = "_" + name;
	    	var d = t in yukkuriSeAPI.__Delay ? yukkuriSeAPI.__Delay[t] : yukkuriSeAPI.__Delay["other"];
	    	notifyMan.Add(d, function(that){
	    		player.play(name);
			}, this);
	    };

	yukkuriSeAPI.ReadyStart = function(time) {
    	player.play("ready");
    };

    yukkuriSeAPI.Ready = function(time) {
//    	player.play("ready");
    };
    yukkuriSeAPI.Start = function() {
    	player.play("start");
    };

    yukkuriSeAPI.Pause = function() {
    	player.play("pause");
    };

    yukkuriSeAPI.Resume = function() {
    	player.play("restart");
    };

    yukkuriSeAPI.End = function() {
    	player.play("gameover");
    };

    yukkuriSeAPI.RokeFirst = function() {
    	yukkuriSeAPI.DelayPlay("roke");
    	yukkuriSeAPI.DelayPlay("madeato");
    	yukkuriSeAPI.DelayPlay("30");
    };

    yukkuriSeAPI.RokeSecond = function() {
    	yukkuriSeAPI.DelayPlay("roke");
    	yukkuriSeAPI.DelayPlay("madeato");
    	yukkuriSeAPI.DelayPlay("15");
    };

    yukkuriSeAPI.RokeFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "roke") {
        	yukkuriSeAPI.DelayPlay("roke");
	    	yukkuriSeAPI.DelayPlay("countdown");
    	} else {
    		// 残り10秒だ
    	}
    };
    yukkuriSeAPI.RokeRespawn = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "roke") {
    		notifyMan.Clear();
    		yukkuriSeAPI.DelayPlay("roke");
    	} else {
    		yukkuriSeAPI.DelayPlay("roke");
    	}

    };
    yukkuriSeAPI.SRFirst = function() {
    	yukkuriSeAPI.DelayPlay("sr");
    	yukkuriSeAPI.DelayPlay("madeato");
    	yukkuriSeAPI.DelayPlay("30");
    };

    yukkuriSeAPI.SRSecond = function() {
    	yukkuriSeAPI.DelayPlay("sr");
    	yukkuriSeAPI.DelayPlay("madeato");
    	yukkuriSeAPI.DelayPlay("15");
    };

    yukkuriSeAPI.SRFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "oursr" || countdownItemName == "theresr") {
        	yukkuriSeAPI.DelayPlay("sr");
	    	yukkuriSeAPI.DelayPlay("countdown");
    	} else {
    		// 残り10秒だ
    	}
    };
    yukkuriSeAPI.SRRespawn = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "oursr" || countdownItemName == "theresr") {
    		notifyMan.Clear();
	    	yukkuriSeAPI.DelayPlay("sr");
    	} else {
	    	yukkuriSeAPI.DelayPlay("sr");
    	}

    };
    yukkuriSeAPI.OSFirst = function() {
    	yukkuriSeAPI.DelayPlay("os");
    	yukkuriSeAPI.DelayPlay("madeato");
    	yukkuriSeAPI.DelayPlay("30");
    };

    yukkuriSeAPI.OSSecond = function() {
    	yukkuriSeAPI.DelayPlay("os");
    	yukkuriSeAPI.DelayPlay("madeato");
    	yukkuriSeAPI.DelayPlay("15");
    };

    yukkuriSeAPI.OSFinal = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "os") {
        	yukkuriSeAPI.DelayPlay("os");
	    	yukkuriSeAPI.DelayPlay("countdown");
    	} else {
    		// 残り10秒だ
    	}
    };
    yukkuriSeAPI.OSRespawn = function() {
    	var countdownItemName = GetCountdownItemFromOptions();
    	if(countdownItemName == "os") {
    		notifyMan.Clear();
	    	yukkuriSeAPI.DelayPlay("os");
    	} else {
    		yukkuriSeAPI.DelayPlay("os");
    	}
    };

    // 試合時間表示の明滅（Blink）を制御する
    // 例）SetBlinkTOGameTime(true); の指定で明滅開始 falseで停止
    var gametimeBlinkTimerID = -1;
	var SetBlinkTOGameTime = function(bBlinkEnable) {
		if(bBlinkEnable == false && gametimeBlinkTimerID != -1) {
			// Blinkを止める
			clearInterval(gametimeBlinkTimerID);
			gametimeBlinkTimerID = -1;
			$("#gametime-process").css("color", "");
		} else {
			// Blinkを開始する
			var data = {
					isTurnOn:true
			};
			gametimeBlinkTimerID = setInterval(function(data){

				if(data.isTurnOn == true) {
					$("#gametime-process").css("color", "#c0c0c0");
					data.isTurnOn = false;
				} else {
					$("#gametime-process").css("color", "");
					data.isTurnOn = true;
				}
			}, 900, data);
		}
	};

    // Start ボタンをクリックした時
	KTLab.Event.AddListener("ui-click-start", applisName, null, function(e){
		KTLab.Event.Queue("game-ready-start", {});
	});

    // Now ボタンをクリックした時
	KTLab.Event.AddListener("ui-click-now", applisName, null, function(e){
		KTLab.Event.Queue("game-start", {});
	});

    // Pause ボタンをクリックした時
	KTLab.Event.AddListener("ui-click-pause", applisName, null, function(e){
		KTLab.Event.Queue("game-pause");
	});

    // Resume ボタンをクリックした時
	KTLab.Event.AddListener("ui-click-resume", applisName, null, function(e){
		KTLab.Event.Queue("game-resume");
	});

    // Reset ボタンをクリックした時
	KTLab.Event.AddListener("ui-click-reset", applisName, null, function(e){
		KTLab.Event.Queue("game-end");
	});

	KTLab.Event.AddListener("ui-click-upbtn", applisName, null, function(e){
	});

	KTLab.Event.AddListener("ui-click-downbtn", applisName, null, function(e){
	});

	KTLab.Event.AddListener("ui-click-allreset", applisName, null, function(e){
		if(window.confirm('状態をリセットします。よろしいですか？')){
			ResetOptions();
			SetupDefault();
		}
	});

	// いずれかの武器のタイマーが有効になったとき
	KTLab.Event.AddListener("weapon-enabled", applisName, this, function(e) {
		options.weapons[e.id].enable = true;
		SaveOptions();
	});

	// いずれかの武器のタイマーが無効になったとき
	KTLab.Event.AddListener("weapon-disabled", applisName, this, function(e) {
		options.weapons[e.id].enable = false;
		SaveOptions();
	});

	KTLab.Event.AddListener("weapon-firstsubtime-changed", applisName, this, function(e) {
		options.weapons[e.id].firstsubtime = e.time;
		SaveOptions();
	});

	KTLab.Event.AddListener("weapon-loopsubtime-changed", applisName, this, function(e) {
		options.weapons[e.id].loopsubtime = e.time;
		SaveOptions();
	});



	// 開始前カウントダウンを付けてタイマーをスタート
	KTLab.Event.AddListener("game-ready-start", applisName, this, function(e) {

		var gt = gameTimer.CreateGameTimer();
		gt.Start();

		$("#timer-captions #before-game").css("display","none");
		$("#timer-captions #after-game").css("display","");

		$("#gametime-process").text(KTLab.MStoTimeString(gameTimer.GetGameTimeMS() + gameTimer.GetReadyTimeMS()));

		$("#resumeBtn").css('display', 'none');
	    $("#pauseBtn").css('display', 'inline-block');

	    $("#settingBtn").hide();

	    $("#startBtn").fadeOut(0);
	    $("#nowBtn").fadeOut(0);

//	    $("#startBtn").css('display', 'none');
//	    $("#nowBtn").css('display', 'none');

	    $("#gametime-process").css('display', 'inline-block');
	    $("#gametime-select").css('display', 'none');
	    $("#select-choice-gametime").selectmenu('disable');

	    var resetBtn = $("#resetBtn");
		resetBtn.css("display", "");
		resetBtn.fadeTo(0, 0.5);
		resetBtn.unbind("click");


		SEAPI.ReadyStart();
	});


	// 開始前カウントダウンを実行中
	KTLab.Event.AddListener("game-ready-interval", applisName, this, function(e) {
		$("#gametime-process").text(KTLab.MStoTimeString(e.time));

		SEAPI.Ready();
	});

	// 開始前カウントダウンが完了
	KTLab.Event.AddListener("game-ready-end", applisName, this, function(e) {
		$("#gametime-process").text(KTLab.MStoTimeString(gameTimer.GetGameTimeMS()));
	});

	// ゲームタイマースタート
	KTLab.Event.AddListener("game-start", applisName, this, function(e) {

		var gt = gameTimer.CreateGameTimerNoReady();
		gt.Start();

		$("#timer-captions #before-game").css("display","none");
		$("#timer-captions #after-game").css("display","");

		$("#gametime-process").text(KTLab.MStoTimeString(gameTimer.GetGameTimeMS()));

		$("#resumeBtn").css('display', 'none');
	    $("#pauseBtn").css('display', 'inline-block');

	    $("#settingBtn").hide();

	    $("#startBtn").fadeOut(0);
	    $("#nowBtn").fadeOut(0);

//	    $("#startBtn").css('display', 'none');
//	    $("#nowBtn").css('display', 'none');
	    $("#gametime-process").css('display', 'inline-block');
	    $("#gametime-select").css('display', 'none');
	    $("#select-choice-gametime").selectmenu('disable');

	    var resetBtn = $("#resetBtn");
		resetBtn.css("display", "");
		resetBtn.fadeTo(0, 0.5);
		resetBtn.unbind("click");


	    KTLab.Event.Queue("game-started", {time:gameTimer.GetGameTimeMS()});
	});

	// ゲームタイマーがスタートした
	KTLab.Event.AddListener("game-started", applisName, this, function(e) {
	 	console.debug("game-started event from app.");

	 	SEAPI.Start();
	 });

	// ゲームタイマー実行中
	KTLab.Event.AddListener("game-interval", applisName, this, function(e) {
		$("#gametime-process").text(KTLab.MStoTimeString(e.time));

	});


	// ゲームタイマーが終了
	KTLab.Event.AddListener("game-end", applisName, null, function(e){

		$("#mainpage").fadeTo(200, 0.5);
		$.mobile.loading('show');

		resetDisplay();

		notifyMan.Clear();

		player.stop();

		SEAPI.End();

		var t = parseInt($("#select-choice-gametime").val()); // UIから時間の初期値を取得
		t = KTLab.MinuteToMS(t);
		KTLab.Event.Queue("game-ended", {time:t});
	});

	// ゲームタイマー終了後
	KTLab.Event.AddListener("game-ended", "app", null, function(e){

		$("#timer-captions #before-game").css("display","");
		$("#timer-captions #after-game").css("display","none");

		var resetBtn = $("#resetBtn");
		resetBtn.css("display", "none");
		resetBtn.unbind("click");

	    $("#settingBtn").show();

	    notifyMan.Start(32);
		// スピナーを表示 src http://docs.monaca.mobi/ja/reference/javascript/utility/
		setTimeout(function()
		{
			$("#mainpage").fadeTo(200, 1.0);
			$.mobile.loading('hide');
		}, 1500);

		SetBlinkTOGameTime(false);
	});



	// ゲームタイマーを再開
	KTLab.Event.AddListener("game-resume", applisName, null, function(e){
		$("#resumeBtn").css('display', 'none');
	    $("#pauseBtn").css('display', 'inline-block');
	    $("#resetBtn").css('display', 'none');

		var resetBtn = $("#resetBtn");
		resetBtn.unbind("click");
		resetBtn.fadeTo(0, 0.5);

		SetBlinkTOGameTime(false);

		setTimeout(function() {
			notifyMan.Start(32);
		}, 1000);

		player.resume();
		SEAPI.Resume();

	//			KTLab.Event.Queue("vib-short", null);
	});




	// ゲームタイマーを一時中断
	KTLab.Event.AddListener("game-pause", applisName, null, function(e){
		$("#resumeBtn").css('display', 'inline-block');
	    $("#pauseBtn").css('display', 'none');
	    $("#resetBtn").css('display', 'inline-block');

		var resetBtn = $("#resetBtn");
		resetBtn.fadeTo(100, 1);
		resetBtn.unbind("click");
		resetBtn.bind("click", function(e){
			KTLab.Event.Queue("ui-click-reset", null);
			e.preventDefault();
		});


//		SEAPI.Pause();
//		notifyMan.Clear();
		notifyMan.Stop();

		player.stop();

		// 試合時間を点滅表示させる
		SetBlinkTOGameTime(true);



		$.mobile.loading('hide');
	//			KTLab.Event.Queue("vib-short", null);
	});

	// 通知要請イベントを受けて通知を実行する
	KTLab.Event.AddListener("weapon-notify-roke-req", applisName, null, function(e){
		switch(e.progress)
		{
		// first
		case 0:
			SEAPI.RokeFirst();
			break;
		// second
		case 1:
			SEAPI.RokeSecond();
			break;
		// final
		case 2:
			SEAPI.RokeFinal();
			break;
		case 3:
			SEAPI.RokeRespawn();
			break;

		}
	});

	KTLab.Event.AddListener("weapon-notify-sr-req", applisName, null, function(e){
		switch(e.progress)
		{
		// first
		case 0:
			SEAPI.SRFirst();
			break;
		// second
		case 1:
			SEAPI.SRSecond();
			break;
		// final
		case 2:
			SEAPI.SRFinal();
			break;
		case 3:
			SEAPI.SRRespawn();
			break;
		}
	});

	KTLab.Event.AddListener("weapon-notify-os-req", applisName, null, function(e){
		switch(e.progress)
		{
		// first
		case 0:
			SEAPI.OSFirst();
			break;
		// second
		case 1:
			SEAPI.OSSecond();
			break;
		// final
		case 2:
			SEAPI.OSFinal();
			break;
		case 3:
			SEAPI.OSRespawn();
			break;
		}
	});
